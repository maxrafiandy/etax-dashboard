<?php

Route::post('user/login', 'UserController@login'); // login new route
Route::post('login', 'UserController@login'); // login old route

// LoggedIn Routes
Route::group(['middleware' => 'auth:api'], function () {
  Route::group(['prefix' => 'dashboard'], function () {
    Route::get('sebaran-jenis-pajak', 'DashboardController@sebaranJenisPajak');
    Route::get('persentasi-offline-online', 'DashboardController@persenOffOn');
    Route::get('offline-online-per-sudin', 'DashboardController@offOnPerSudin');
    Route::get('offline-online-per-sudin/{kode_sudin}', 'DashboardController@getOpSudin');
    Route::get('status-per-sudin', 'DashboardController@offlineOnlineSudin');
    Route::get('status-per-sudin/{kode_sudin}', 'DashboardController@offlineOnlineSudin');
    Route::get('last-transaction', 'DashboardController@getLastTransaction');
    Route::get('top-omset', 'DashboardController@getTopOmset');
    Route::get('top-autodebet', 'DashboardController@getTopAutodebet');
    Route::get('omset-jenis-usaha/{kode_jenis_usaha}', 'DashboardController@getOmsetJenisUsaha');
    Route::get('omset-sudin', 'DashboardController@getOmsetSudin');
  });

  Route::group(['prefix' => 'offline-devices'], function () {
    Route::get('sudin/{kode_sudin}', 'OfflineDeviceController@offlineDevices');
    Route::post('bap/upload', 'OfflineDeviceController@uploadBap');
    Route::get('bap/download/{id}', 'OfflineDeviceController@getBap');
    Route::get('history/{nopd}', 'OfflineDeviceController@getOfflineHistory');
  });

  Route::group(['prefix' => 'double-debet'], function () {
    Route::get('/', 'DoubleDebetController@doubleDebet');
  });

  Route::get('availability/koordinat-wp/{kode_sudin}', 'KoordinatWpController@koordinatWp');

  Route::get('transaksi-autodebet/sudin/{kode_sudin}/status/{status}', 'AutodebetController');
  Route::get('progres-omset/sudin/{kode_sudin}', 'ProgresOmsetController@progresOmsetSudin');
  Route::get('progres-omset/nopd/{nopd}', 'ProgresOmsetController@detailOmset');
  Route::get('penarikan/sudin/{kode_sudin}', 'PenarikanController');

  // Users new routes
  Route::group(['prefix' => 'user'], function () {
    Route::get('details', 'UserController@details');
    Route::get('logout', 'UserController@logout');
    Route::post('register', 'UserController@register');
    Route::put('update-password', 'UserController@updatePassword');
  });

  // Users old routes
  Route::get('user-details', 'UserController@details');
  Route::put('update-password', 'UserController@updatePassword');
  Route::post('register', 'UserController@register');
  Route::get('logout', 'UserController@logout');
});

// Route::group(['prefix' => 'offline-devices'], function () {
//   Route::get('bap/download/{id}', 'OfflineDeviceController@getBap');
// });

Route::group(['prefix' => 'v2'], function () {
  Route::group(['prefix' => 'sudin'], function () {
    Route::get('omset', 'v2\SudinController@omset');
    Route::get('{kode_sudin}/omset', 'v2\SudinController@omset');
    Route::get('total-objek-pajak', 'v2\SudinController@totalOp');
    Route::get('{kode_sudin}/total-objek-pajak', 'v2\SudinController@totalOp');
  });

  Route::group(['prefix' => 'jenis-usaha'], function () {
    Route::get('omset', 'v2\JenisUsahaController@omset');
    Route::get('{kode_jenis_usaha}/omset', 'v2\JenisUsahaController@omset');
    Route::get('total-objek-pajak', 'v2\JenisUsahaController@totalOp');
    Route::get('{kode_jenis_usaha}/total-objek-pajak', 'v2\JenisUsahaController@totalOp');
  });
});
