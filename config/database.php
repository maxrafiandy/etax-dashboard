<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DEFAULT_CONNECTION', 'monitoring'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        // konfigurasi koneksi db DWH (monitoring)
        'monitoring' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DWH_HOST', '172.17.0.1'),
            'port' => env('DWH_PORT', '1433'),
            'database' => env('MONITORING_DATABASE', 'monitoring'),
            'username' => env('MONITORING_USERNAME', 'sa'),
            'password' => env('MONITORING_PASSWORD', 'p@ssw0rd'),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        // konfigurasi koneksi db DWH (MASTER_PAJAK_ONLINE_DKI)
        'mpod' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DWH_HOST', '172.17.0.1'),
            'port' => env('DWH_PORT', '1433'),
            'database' => env('MPOD_DATABASE', 'MASTER_PAJAK_ONLINE_DKI'),
            'username' => env('MPOD_USERNAME', 'sa'),
            'password' => env('MPOD_PASSWORD', 'p@ssw0rd'),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        // konfigurasi koneksi db DWH (PAJAK_ONLINE_DKI)
        'pod' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DWH_HOST', '172.17.0.1'),
            'port' => env('DWH_PORT', '1433'),
            'database' => env('POD_DATABASE', 'PAJAK_ONLINE_DKI'),
            'username' => env('POD_USERNAME', 'sa'),
            'password' => env('POD_PASSWORD', 'p@ssw0rd'),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        // konfigurasi koneksi db DWH (etax_grabber)
        'etaxgrabber' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DWH_HOST', '172.17.0.1'),
            'port' => env('DWH_PORT', '1433'),
            'database' => env('ETAX_GRABBER_DATABASE', 'etax_grabber'),
            'username' => env('ETAX_GRABBER_USERNAME', 'sa'),
            'password' => env('ETAX_GRABBER_PASSWORD', 'p@ssw0rd'),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        // konfigurasi koneksi db CMS (brichannel)
        'brichannel' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('CMS_HOST', '172.17.0.1'),
            'port' => env('CMS_PORT', '3306'),
            'database' => env('BRICHANNEL_DATABASE', 'brichannel'),
            'username' => env('BRICHANNEL_USERNAME', 'root'),
            'password' => env('BRICHANNEL_PASSWORD', 'p@ssw0rd'),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'predis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'predis'),
            'prefix' => Str::slug(env('APP_NAME', 'laravel'), '_').'_database_',
        ],

        'default' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

        'cache' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_CACHE_DB', 1),
        ],

    ],

    'monitoring' => env('MONITORING_DATABASE', 'monitoring_new'),
    'mpod' => env('MPOD_DATABASE', 'MASTER_PAJAK_ONLINE_DKI'),
    'pod' => env('POD_DATABASE', 'PAJAK_ONLINE_DKI'),
    'etax_grabber' => env('ETAX_GRABBER_DATABASE', 'etax_grabber')

];
