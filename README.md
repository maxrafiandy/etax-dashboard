# Dependencies
[+] php (Minimum _v7.2_, Recommended _v7.3_)
[+] php-sysbase, php-odbc, php-mysql (depends on php version)
[+] nodejs (_v10.16.0_ with npm _v6.9.0_)
[+] composer (_v1.8.4_)
[+] Laravel 5.8

# How to
1. Clone the code.
2. run: **composer install && npm install**
3. run: **php artisan passport:install**
4. run: **php artisan storage:link**
5. run: **php artisan migrate** _(first install only)_
6. Config the **.env** file to appropriate configuration
7. run: **php artisan route:cache**
8. run: **php artisan config:cache**
9. run: **php artisan cache:clear**
10. run: **npm run prod**
11. Set **public** as root directory to webserver (**nginx** or **apache**)