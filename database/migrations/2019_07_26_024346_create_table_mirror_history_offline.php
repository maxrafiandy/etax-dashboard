<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMirrorHistoryOffline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_mirror_history_offline', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nopd', 20);
            $table->string('npwpd', 20);
            $table->string('nama_objek_usaha')->nullable();
            $table->string('nama_wajib_pajak')->nullable();
            $table->string('alamat')->nullable();
            $table->string('kode_sudin', 2);
            $table->string('jenis_usaha')->nullable();
            $table->string('jenis_penarikan')->nullable();
            $table->string('ipaddress')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('status')->nullable();
            $table->datetime('last_availability')->nullable();
            $table->datetime('last_reliable')->nullable();
            $table->timestamps();

            $table->index(['nopd']);
            $table->index(['npwpd']);
            $table->index(['kode_sudin']);
            $table->index(['kode_sudin', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_mirror_history_offline');
    }
}
