<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMirrorAutodebet extends Migration
{
  public function up()
  {
    Schema::create('tbl_mirror_autodebet', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('nopd', 20);
      $table->smallInteger('status');
      $table->decimal('autodebet_1', 20, 2);
      $table->decimal('autodebet_2', 20, 2);
      $table->decimal('autodebet_3', 20, 2);
      $table->string('masapajak', 2);
      $table->string('tahunpajak', 4);
      $table->timestamps();

      $table->index(['status']);
    });
  }

  public function down()
  {
    Schema::dropIfExists('tbl_mirror_autodebet');
  }
}
