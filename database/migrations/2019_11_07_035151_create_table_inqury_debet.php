<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInquryDebet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_inquiry_debet', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_rek',20);
            $table->string('desk_tran', 100);
            $table->decimal('saldo_awal_mutasi', 15,2);
            $table->decimal('saldo_akhir_mutasi', 15,2);
            $table->decimal('mutasi_debet', 15,2);
            $table->decimal('mutasi_kredit', 15,2);
            $table->smallInteger('kode_tran');
            $table->date('tgl_tran');
            $table->integer('jam_tran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_inquiry_debet');
    }
}
