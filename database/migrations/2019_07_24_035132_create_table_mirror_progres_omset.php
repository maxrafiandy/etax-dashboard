<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMirrorProgresOmset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_mirror_progres_omset', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nopd', 20);
            $table->string('nama_objek_pajak');
            $table->string('kode_sudin', 2);
            $table->decimal('trx1', 20, 2);
            $table->decimal('omset1', 20, 2);
            $table->decimal('pajak1', 20, 2);
            $table->decimal('trx2', 20, 2);
            $table->decimal('omset2', 20, 2);
            $table->decimal('pajak2', 20, 2);
            $table->decimal('trx3', 20, 2);
            $table->decimal('omset3', 20, 2);
            $table->decimal('pajak3', 20, 2);
            $table->string('masapajak', 2);
            $table->string('tahunpajak', 4);
            $table->timestamps();

            $table->index(['kode_sudin']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_mirror_progres_omset');
    }
}
