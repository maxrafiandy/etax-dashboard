<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLaporanOffline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_laporan_offline', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nopd', 20);
            $table->string('attachment'); // file bap
            $table->string('keterangan')->nullable();            
            $table->timestamps();

            $table->index(['nopd']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_laporan_offline');
    }
}
