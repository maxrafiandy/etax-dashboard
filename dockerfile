FROM wyveo/nginx-php-fpm:php73

ENV TZ Asia/Jakarta

RUN apt-get update \
  && apt-get install php7.3-mysql \
  php7.3-zip php7.3-xml php7.3-mbstring \
  php7.3-bcmath php7.3-common \
  php7.3-odbc php7.3-pgsql php7.3-sybase \
  php7.3-json php7.3-tokenizer \
  php7.3-curl php7.3-cli \
  software-properties-common -y \
  --no-install-recommends --no-install-suggests
  
RUN apt-get update \
  && apt-get -y install lsb-release apt-transport-https ca-certificates \
  wget curl gnupg make -y --no-install-recommends --no-install-suggests \
  && curl -ksL https://deb.nodesource.com/setup_10.x | bash - \
  && curl -k https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
  && curl -k https://packages.microsoft.com/config/debian/8/prod.list > /etc/apt/sources.list.d/mssql-release.list \
  && apt-get update \
  && ACCEPT_EULA=Y apt-get install \
  g++ unixodbc unixodbc-dev \
  msodbcsql17 mssql-tools -y \
  --no-install-recommends --no-install-suggests

RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile \
  && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc

RUN pecl install sqlsrv pdo_sqlsrv \
  && printf "; priority=20\nextension=sqlsrv.so\n" > /etc/php/7.3/mods-available/sqlsrv.ini \
  && printf "; priority=30\nextension=pdo_sqlsrv.so\n" > /etc/php/7.3/mods-available/pdo_sqlsrv.ini \
  && phpenmod -v 7.3 sqlsrv pdo_sqlsrv

RUN apt-get autoremove -y \
  && apt-get clean \
  && apt-get autoclean \
  && rm -rf /var/lib/apt/lists/* \
  && rm -rf /usr/share/man/?? \
  && rm -rf /usr/share/man/??_* \
  && rm -rf /var/cache/apt/*

COPY server.conf /etc/nginx/conf.d/sites-available/default