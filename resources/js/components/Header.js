import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { signout } from '../actions/AuthActions';
import { appLogout } from '../services/AppService';

const Header = (props) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const logout = () => {
    const resp = appLogout();
    resp.promise.then(resp => {
      localStorage.removeItem("token");
      // dispatch(signout())
      dispatch({ type: "SIGN_OUT" })
    });
  }

  return (
    <header className="main-header">
      <Link to="/" className="logo">
        <span className="logo-mini"><b>etax</b></span>
        <span className="logo-lg pull-left"><img src="images/etax-dki.png" width="182" height="46" /></span>
      </Link>

      <nav className="navbar navbar-static-top">
        <a href="#" className="sidebar-toggle" data-toggle="push-menu" role="button">
          <span className="sr-only">Toggle navigation</span>
        </a>
        <div className="navbar-custom-menu">
          <ul className="nav navbar-nav">
            <li className="dropdown user user-menu">
              <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                <img src="adminlte/dist/img/avatar5.png" className="img-circle user-image" alt="User Image" />
                <span className="hidden-xs">{auth.username}</span>
              </a>
              <ul className="dropdown-menu">
                <li className="user-header bg-info">
                  <img src="adminlte/dist/img/avatar5.png" className="img-circle" alt="User Image" />
                  <p>{auth.username}</p>
                </li>
                <li className="user-footer">
                  <div className="pull-left">
                    <Link to="/update-password" className="btn btn-success btn-block btn-flat">Ubah Password</Link>
                  </div>
                  <div className="pull-right">
                    <button type="button" className="btn btn-warning btn-block btn-flat"
                      onClick={logout}>Sign out
                    </button>
                  </div>
                </li>
              </ul>
            </li>
            {/* <li>
              <a href="#" data-toggle="modal" data-target="#modal-logout"><i className="fa fa-sign-out"></i> Sign out</a>
            </li> */}
          </ul>
        </div>
      </nav>
    </header>
  );
}

export default Header;
