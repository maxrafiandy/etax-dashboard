import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Sidebar = () => {
  const prefix = "";
  const auth = useSelector((state) => state.auth);
  return (
    <aside className="main-sidebar">
      <section className="sidebar">
        <ul className="sidebar-menu" data-widget="tree">
          {/* <li className="header">MAIN</li>
          <li><Link to={prefix + "/"}><i className="fa fa-dashboard"></i> <span>Dashboard</span></Link></li>
          <li><Link to={prefix + "/availability-perangkat"}><i className="fa fa-map-pin"></i> <span>Availability</span></Link></li>
          <li className="header">REPORT</li>
          <li><Link to={prefix + "/laporan-autodebet"}><i className="fa fa-credit-card"></i> <span>Lap. Autodebet</span></Link></li>
          <li><Link to={prefix + "/laporan-progres-omset"}><i className="fa fa-line-chart "></i> <span>Lap. Progress Omset</span></Link></li>
          <li><Link to={prefix + "/laporan-penarikan"}><i className="fa fa-money"></i> <span>Lap. Penarikan</span></Link></li>
          <li className="header">MAINTENANCE</li>
          <li><Link to={prefix + "/laporan-offline"}><i className="fa fa-exclamation-triangle"></i> <span>Perangkat Offline</span></Link></li>
          {
            auth.level === "ADMIN" && <li><Link to="/register"><i className="fa fa-users"></i> <span>Register User</span></Link></li>
          } */}
          {auth.menu.map((m,i) => {
            if (m.type == "header") return <li key={i} className={m.type}>{m.label}</li>
            return <li key={i}><Link to={prefix + m.url}><i className={m.icon}></i> <span>{m.label}</span></Link></li>
          })}
        </ul>
      </section>
    </aside>
  );
}

export default Sidebar;