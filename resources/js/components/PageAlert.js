import React, { Component } from 'react';

class PageAlert extends Component {

  constructor(props) {
    super(props);
    this.state = {
      type: props.type ? props.type :  "warning",
      icon: props.icon ? props.icon : "fa fa-warning",
      title: props.title ? props.title : "Warning!",
      message: props.message ? props.message : "Something may went wrong."
    }
  }

  render() {
    return (
      <div className={`alert alert-${this.state.type} alert-dismissible`}>
        <button type="button" className="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4>
          <i className={`icon ${this.state.icon}`}></i>
          {this.state.title}
        </h4>
        {this.state.message}
      </div>
    );
  }
}

export default PageAlert;