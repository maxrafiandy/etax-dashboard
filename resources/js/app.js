/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//  components
require('./components/Header');
require('./components/Sidebar');
require('./components/ControlSidebar');
require('./components/PageAlert');

// pages
require('./pages/App');
require('./pages/Dashboard');
require('./pages/Availability');
require('./pages/ReportAutodebet');
require('./pages/ReportOffline');
require('./pages/ReportOfflineDetail');
require('./pages/ReportPenarikan');
require('./pages/ReportProgresOmset');
require('./pages/ReportDoubleDebet');
require('./pages/Register');
require('./pages/UpdatePassword');

// redux components
require('./actions/AuthActions');
require('./reducers/index'); // all reducers combined here
require('./store');