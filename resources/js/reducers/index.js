import { combineReducers } from 'redux';
import auth from './AuthReducer';
import dashboard from './DashboardReducer';
import double_debet from './ReportDoubleDebetReducer';
const reducers = combineReducers({
  auth,
  dashboard,
  double_debet
});
export default reducers;