const dstate = {
  logged_in: false,
  token: null,
  username: null,
  level: null,
  menu: null,
  wilayah: [],
  loading: false
}

const auth = (state = dstate, { type, payloads }) => {
  switch (type) {
    case 'SIGN_IN':
      return {
        ...state,
        logged_in: true,
        token: payloads.token,
        username: payloads.username,
        level: payloads.level,
        menu: payloads.menu,
        wilayah: payloads.wilayah
      };
    case 'SIGN_OUT':
      return {
        ...state,
        logged_in: false,
        token: null,
        username: null,
        level: null,
        menu: null,
        wilayah: []
      }
    case 'AUTH_SET_LOADING':
      return {
        ...state,
        loading: true
      }
    case 'AUTH_UNSET_LOADING':
      return {
        ...state,
        loading: false
      }
    default: return state;
  }
}

export default auth;