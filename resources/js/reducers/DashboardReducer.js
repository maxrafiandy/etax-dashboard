const dstate = {
  omset_sudin: [],
  omset_hotel: 0,
  op_hotel: 0,
  omset_resto: 0,
  op_resto: 0,
  omset_hiburan: 0,
  op_hiburan: 0,
  omset_parkir: 0,
  op_parkir: 0,
  top_autodebet: [],
  top_omset: [],
  status_per_sudin: [],
  op_online: 0,
  op_offline: 100
}

const dashboard = (state = dstate, { type, payloads }) => {
  switch (type) {
    case 'SET_OMSET_SUDIN': {
      return {
        ...state,
        omset_sudin: payloads
      }
    }
    case 'SET_OMSET_HOTEL':
      return {
        ...state,
        omset_hotel: payloads
      };
    case 'SET_OMSET_RESTO':
      return {
        ...state,
        omset_resto: payloads
      };
    case 'SET_OMSET_HIBURAN':
      return {
        ...state,
        omset_hiburan: payloads
      };
    case 'SET_OMSET_PARKIR':
      return {
        ...state,
        omset_parkir: payloads
      };
    case 'SET_OP_HOTEL':
      return {
        ...state,
        op_hotel: payloads
      };
    case 'SET_OP_RESTO':
      return {
        ...state,
        op_resto: payloads
      };
    case 'SET_OP_HIBURAN':
      return {
        ...state,
        op_hiburan: payloads
      };
    case 'SET_OP_PARKIR':
      return {
        ...state,
        op_parkir: payloads
      };
    case 'SET_TOP_AUTODEBET':
      return {
        ...state,
        top_autodebet: payloads
      }
    case 'SET_TOP_OMSET':
      return {
        ...state,
        top_omset: payloads
      }
    case 'SET_STATUS_PER_SUDIN':
      return {
        ...state,
        status_per_sudin: payloads
      }
    case 'SET_OP_ONLINE':
      return {
        ...state,
        op_online: payloads
      }
    case 'SET_OP_OFFLINE':
      return {
        ...state,
        op_offline: payloads
      }
    default: return state;
  }
}

export default dashboard;