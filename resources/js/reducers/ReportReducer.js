const report = (state = {}, { type, payloads }) => {
  switch (type) {
    case 'AUTODEBET':
      return {
        ...state,
        autodebet: payloads.autodebet,
        months: payloads.months,
        years: payloads.years
      };
    case 'OFFLINE':
      return {
        ...state,
        offline: payloads.offline
      };
    case 'OMSET':
      return {
        ...state,
        omset: payloads.omset
      };
    case 'PENARIKAN':
      return {
        ...state,
        penarikan: payloads.penarikan
      };
    default: return state;
  }
}

export default report;