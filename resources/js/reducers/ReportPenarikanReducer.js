const defaultState = {
  datatable: [],
  year: 0,
  month: 0
}

const reportPenarikan = (state = defaultState, { type, payloads }) => {
  switch (type) {
    case 'SET_DATA':
      return {
        ...state,
        datatable: payloads.penarikan,
        year: payloads.year,
        month: payloads.month
      };
    default: return state;
  }
}

export default reportPenarikan;