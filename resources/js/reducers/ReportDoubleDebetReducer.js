const iState = {
  kode_sudin: "01",
  loading: true,
  double_debet: [],
  alerts: []
}

const double_debet = (state = iState, { type, payloads }) => {
  switch (type) {
    case 'DDEBET_SET_LOADING':
      return {
        ...state,
        loading: true
      }

    case 'DDEBET_UNSET_LOADING':
      return {
        ...state,
        loading: false
      }

    case 'DDEBET_SET_DATA':
      return {
        ...state,
        double_debet: payloads.double_debet,
        loading: false
      }

    case 'DDEBET_SET_ALERTS':
      return {
        ...state,
        alerts: payloads.alerts
      }

    case 'DDEBET_UNSET_ALERTS':
      return {
        ...state,
        alerts: []
      }

    default: return state;
  }
}

export default double_debet;