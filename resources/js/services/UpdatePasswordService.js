import axios from 'axios';
import store from '../store';
import qs from 'qs';

const url = "api/user/update-password";

function putData(data) {
  const auth = store.getState().auth;
  const config = { headers: { "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer " + auth.token } };
  
  return axios.put(url, qs.stringify(data), config)
      .then(resp => resp.data)
};

export default putData;