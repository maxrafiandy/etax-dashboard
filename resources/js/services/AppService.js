import axios from 'axios';
import store from '../store';
import { LOGOUT_URL, LOGIN_URL } from './Routes';

const dispatch = store.dispatch;

export const appLogin = ({ email, password, remember_me }) => {
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { cancelToken: source.token };

  const promise = axios.post(LOGIN_URL, { email, password, remember_me }, config)
    .then(resp => {
      const { token, username, level, menu, wilayah } = resp.data;
      const payloads = { token, username, level, menu, wilayah };
      dispatch({ type: 'SIGN_IN', payloads });
      localStorage.setItem("token", token);
      return payloads;
    })
  return { promise, source }
}

export const appLogout = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const promise = axios.get(LOGOUT_URL, config)
    .then(resp => resp.data);
  return { promise, source }
}