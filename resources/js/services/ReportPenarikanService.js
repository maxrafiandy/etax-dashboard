import axios from 'axios';
import store from '../store';

export const getPenarikan = (kode_sudin) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/penarikan/sudin/${kode_sudin}`;
  // const promise = new Promise((resolve, reject) => {
  //   axios.get(url, config)
  //   .then(resp => resolve(resp.data))
  //   .catch(err => reject(err));
  // });

  const promise = axios.get(url, config)
    .then(resp => resp.data);
  return { promise, source };
};