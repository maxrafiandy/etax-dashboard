import axios from 'axios';
import store from '../store';

class RequestConfigService {
  static getDefaultConfig() {
    const auth = store.getState().auth;
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    return {
      config: {
        headers: {
          "Authorization": "Bearer " + auth.token,
          "Content-Type": "application/json",
          "Accept": "application/json"
        }, cancelToken: source.token
      },
      source
    };
  }

  static getToken() {
    return store.getState().auth.token;
  }


}

export default RequestConfigService;