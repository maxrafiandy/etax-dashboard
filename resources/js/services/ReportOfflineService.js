import axios from 'axios';
import store from '../store';
import { agents } from '../master';

export const getOffline = (kode_sudin) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = "api/offline-devices/sudin";
  // Don't delete
  // const promise = new Promise((resolve, reject) => {
  //   axios.get(`${url}/${kode_sudin}`, config)
  //     .then(resp => resolve(resp.data))
  //     .catch(err => {
  //       if (axios.isCancel()) console.error("Request cancelled: ", err);
  //       else reject(err);
  //     });
  // });
  const promise = axios.get(`${url}/${kode_sudin}`, config)
    .then(resp => resp.data);
  return ({ promise, source });
};

export const getOfflineHistory = (nopd) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = "api/offline-devices/history";
  const promise = axios.get(`${url}/${nopd}`, config)
    .then(resp => resp.data);
  return ({ promise, source });
}

export const uploadBap = ({ nopd, file, keterangan }) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token, "Content-Type": "multipart/form-data" }, cancelToken: source.token };
  const url = "api/offline-devices/bap/upload";
  const request = new FormData();
  request.append("nopd", nopd);
  request.append("file", file);
  request.append("keterangan", keterangan);
  // Don't delete
  // const promise = new Promise((resolve, reject) => {
  //   axios.post(url, request, config)
  //     .then(resp => resolve(resp.data))
  //     .catch(err => {
  //       if (axios.isCancel()) console.error("Request cancelled: ", err);
  //       else reject(err)
  //     });
  // });
  const promise = axios.post(url, request, config)
    .then(resp => resp.data);
  return ({ promise, source });
}

export const downloadBap = (id) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token, responseType: "blob" };
  const url = `api/offline-devices/bap/download/${id}`;
  const promise = axios.get(url, config)
      .then(resp => resp.data);
  return ({promise, source});
}