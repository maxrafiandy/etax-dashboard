import axios from 'axios';
import store from '../store';
import qs from 'qs';

const url = "api/user/register";

export const postData = (data) => {
  const auth = store.getState().auth;
  const config = { headers: { "Content-Type": "application/json", "Authorization": "Bearer " + auth.token } };
  return axios.post(url, data, config)
};