export const LOGIN_URL = "api/user/login";
export const LOGOUT_URL = "api/user/logout";
export const SEBARAN_JENIS_PAJAK_URL = "api/dashboard/sebaran-jenis-pajak";
export const PERSENTASI_OFFLINE_ONLINE_URL = "api/dashboard/persentasi-offline-online";
export const STATUS_PER_SUDIN_URL = "api/dashboard/status-per-sudin";