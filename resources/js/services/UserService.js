import axios from 'axios';
// import store from '../store';

export const userDetails = (token) => {
  // const auth = store.getState().auth;
  const config = { headers: { "Content-Type": "application/json", "Authorization": "Bearer " + token } };
  const url = "api/user/details";

  return axios.get(url, config).then(resp => resp.data);
};