import axios from 'axios';
import RequestConfigService from './ConfigService';

class DoubleDebetService {
  static doubleDebet() {
    const url = `api/double-debet`;
    const { config, source } = RequestConfigService.getDefaultConfig();
    const promise = axios.get(url, config);
    return ({ promise, source });
  }
}

export default DoubleDebetService;