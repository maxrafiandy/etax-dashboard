import axios from 'axios';
import store from '../store';
import { agents } from '../master';

export const getAgentStatus = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = {
    headers: {
      "Authorization": "Bearer " + auth.token
    }, cancelToken: source.token
  };
  const url = `${agents.status.index}`;
  const promise = axios.get(url, config)
    .then(resp => {
      const entries = Object.entries(resp.data.agents);
      var sudin = [0,0,0,0,0];
      for(const [k, v] of entries) {
        switch(v.kode_sudin) {
          case "1": v.status ? sudin[0] += 1 : ''; break;
          case "3": v.status ? sudin[1] += 1 : ''; break;
          case "5": v.status ? sudin[2] += 1 : ''; break;
          case "7": v.status ? sudin[3] += 1 : ''; break;
          case "9": v.status ? sudin[4] += 1 : ''; break;
        }
      };
      return sudin;
    });
  return { promise, source };
}