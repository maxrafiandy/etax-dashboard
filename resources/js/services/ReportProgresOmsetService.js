import axios from 'axios';
import store from '../store';

import { useDispatch, useSelector } from 'react-redux';

export const getProgresOmset = (kode_sudin) => {  
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/progres-omset/sudin/${kode_sudin}`;
  const promise = axios.get(url, config)
    .then(resp => resp.data);
  return ({ promise, source });
};

export const getDetailProgresOmset = (nopd) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/progres-omset/nopd/${nopd}`;
  const promise = axios.get(url, config)
    .then(resp => resp.data);
  return ({ promise, source });
};