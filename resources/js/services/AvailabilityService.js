import axios from 'axios';
import store from '../store';
import Color from '../components/Color';

const n = Number;

export const getKoordinatWp = (kode_sudin) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/availability/koordinat-wp/${kode_sudin}`;

  const promise = axios.get(url, config)
    .then((resp) => {
      var koordinat_wp = resp.data.koordinat_wp;
      var off_devices = "", on_devices = "";
      koordinat_wp.map((v, i) => {
        v.status == 1 ? on_devices += `${v.alamat} (IP: ${v.ipaddress ? v.ipaddress : '-'} ) | ` :
          off_devices += `${v.alamat} (IP: ${v.ipaddress ? v.ipaddress : '-'}) | `;
      });
      return { koordinat_wp, on_devices, off_devices };
    });
  return { promise, source };
}