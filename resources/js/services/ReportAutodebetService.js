import axios from 'axios';
import store from '../store';

export const getAutodebet = (kode_sudin, status) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/transaksi-autodebet/sudin/${kode_sudin}/status/${status}`;
  // const promise = new Promise((resolve, reject) => {
  //   axios.get(url, config)
  //     .then(resp => {
  //       const data = resp.data;
  //       resolve({ autodebet: data.nopds, months: data.months, years: data.years });
  //     })
  //     .catch(err => { reject(err) });
  // });

  const promise = axios.get(url, config)
      .then(resp => {
        const data = resp.data;
        return { autodebet: data.nopds, months: data.months, years: data.years };
      });
  return { promise, source }
}