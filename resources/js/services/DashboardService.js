import axios from 'axios';
import store from '../store';
import Color from '../components/Color';
import { SEBARAN_JENIS_PAJAK_URL, PERSENTASI_OFFLINE_ONLINE_URL, STATUS_PER_SUDIN_URL } from './Routes';

const n = Number;

export const getSebaranJenisPajak = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  // const url = "api/dashboard/sebaran-jenis-pajak";
  const promise = axios.get(SEBARAN_JENIS_PAJAK_URL, config)
    .then(res => {
      const data = res.data;
      const sum = { resto: 0, parkir: 0, hiburan: 0, hotel: 0, total: 0 }; // response value
      data.sebaran_jenis_pajak.map(d => {
        switch (d.kode_jenis_usaha) {
          case "R": sum.resto = n(d.jumlah_jenis_usaha); break;
          case "P": sum.parkir = n(d.jumlah_jenis_usaha); break;
          case "H": sum.hiburan = n(d.jumlah_jenis_usaha); break;
          case "T": sum.hotel = n(d.jumlah_jenis_usaha); break;
        }
        sum.total += n(d.jumlah_jenis_usaha);
      });
      return sum;
    });
  return { promise, source };
}

export const getPersenOfflineOnline = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  // const url = "api/dashboard/persentasi-offline-online";
  const promise = axios.get(PERSENTASI_OFFLINE_ONLINE_URL, config)
    .then(resp => {
      const response = { off_online: [], dataset: [] }; // response value;
      const data = resp.data;
      const colors = [Color.warning, Color.success, Color.info];
      response.off_online = data.persen_off_on;
      response.off_online.map((v, i) => {
        const dataset = { label: v.keterangan, backgroundColor: colors[i], data: [v.jumlah_status_live] };
        response.dataset.push(dataset);
      });
      return response;
    });

  return { promise, source };
}

export const getStatusPerSudin = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  // const url = "api/dashboard/status-per-sudin";
  const promise = axios.get(STATUS_PER_SUDIN_URL, config)
    .then(resp => {
      return resp.data.status_sudin
    });
  return { promise, source };
}

export const getOpSudin = (kode_sudin) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/dashboard/offline-online-per-sudin/${kode_sudin}`;
  const promise = axios.get(url, config)
    .then(resp => resp.data.sudin);
  return { promise, source };
}

export const getLastTransaction = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/dashboard/last-transaction`;
  const promise = axios.get(url, config)
    .then(resp => resp.data.last_transaction);
  return { promise, source };
}

export const getTopAutodebet = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/dashboard/top-autodebet`;
  const promise = axios.get(url, config)
    .then(resp => resp.data.top_autodebet);
  return { promise, source };
}

export const getTopOmset = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/dashboard/top-omset`;
  const promise = axios.get(url, config)
    .then(resp => resp.data.top_omset);
  return { promise, source };
}

export const getOmsetSudin = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/dashboard/omset-sudin`;
  const promise = axios.get(url, config)
    .then(resp => resp.data.omset);
  return { promise, source };
}

export const getOmsetJenisUsaha = (kode_jenis_usaha) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/dashboard/omset-jenis-usaha/${kode_jenis_usaha}`;
  const promise = axios.get(url, config)
    .then(resp => resp.data.total_omset);
  return { promise, source };
}

/** v2 **/
const dispatch = store.dispatch;

export const omsetJenisUsaha = (kode_jenis_usaha) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/v2/jenis-usaha/${kode_jenis_usaha}/omset`;
  axios.get(url, config)
    .then(resp => {
      const payloads = resp.data.omset;
      switch (kode_jenis_usaha) {
        case 'T': dispatch({ type: 'SET_OMSET_HOTEL', payloads }); return;
        case 'H': dispatch({ type: 'SET_OMSET_HIBURAN', payloads }); return;
        case 'P': dispatch({ type: 'SET_OMSET_PARKIR', payloads }); return;
        case 'R': dispatch({ type: 'SET_OMSET_RESTO', payloads }); return;
      }
    });
  return source;
}

export const opJenisUsaha = (kode_jenis_usaha) => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/v2/jenis-usaha/${kode_jenis_usaha}/total-objek-pajak`;
  axios.get(url, config)
    .then(resp => {
      const payloads = resp.data.total_op;
      switch (kode_jenis_usaha) {
        case 'T': dispatch({ type: 'SET_OP_HOTEL', payloads }); return;
        case 'H': dispatch({ type: 'SET_OP_HIBURAN', payloads }); return;
        case 'P': dispatch({ type: 'SET_OP_PARKIR', payloads }); return;
        case 'R': dispatch({ type: 'SET_OP_RESTO', payloads }); return;
      }
    })
    .catch(err => console.log(`error@${url}`, err));
  return source;
}

export const omsetSudin = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/v2/sudin/omset`;
  axios.get(url, config)
    .then(resp => dispatch({ type: 'SET_OMSET_SUDIN', payloads: resp.data.omsets }))
    .catch(err => console.log(`error@${url}`, err));
  return source;
}

export const topAutodebet = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/dashboard/top-autodebet`;
  axios.get(url, config)
    .then(resp => dispatch({ type: 'SET_TOP_AUTODEBET', payloads: resp.data.top_autodebet }))
    .catch(err => console.log(`error@${url}`, err));
  return source;
}

export const topOmset = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = `api/dashboard/top-omset`;
  axios.get(url, config)
    .then(resp => dispatch({ type: 'SET_TOP_OMSET', payloads: resp.data.top_omset }))
    .catch(err => console.log(`error@${url}`, err));
  return source;
}

export const statusPerSudin = () => {
  const auth = store.getState().auth;
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  const config = { headers: { "Authorization": "Bearer " + auth.token }, cancelToken: source.token };
  const url = "api/dashboard/status-per-sudin";
  axios.get(url, config)
    .then(resp => {
      const status_sudin = resp.data.status_sudin;
      dispatch({ type: 'SET_STATUS_PER_SUDIN', payloads: status_sudin });

      const online = status_sudin.reduce(function (prev, cur) {
        return n(prev) + n(cur.jumlah_online);
      }, 0);

      const offline = status_sudin.reduce(function (prev, cur) {
        return n(prev) + n(cur.jumlah_offline);
      }, 0);

      dispatch({ type: 'SET_OP_ONLINE', payloads: online});
      dispatch({ type: 'SET_OP_OFFLINE', payloads: offline});
    })
    .catch(err => console.log(`error@${url}`, err));
  return source;
}

/*# v2 #*/