export const signin = (payloads) => {
    return {
        type: 'SIGN_IN',
        payloads
    }
}

export const signout = (payloads) => {
    return {
        type: 'SIGN_OUT',
        payloads
    }
}