export const onPageLoad = (payloads) => {
  return {
    type: 'PAGE_LOAD',
    payloads
  }
}

export const onButonFilterClicked = (payloads) => {
  return {
    type: 'FILTER_CLICK',
    payloads
  }
}

export const ReportDoubleDebetActions = (type, payloads) => {
  return {
    type, payloads
  } 
}