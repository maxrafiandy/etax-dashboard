export const setAutodebets = (payloads) => {
  return {
    type: 'AUTODEBET',
    payloads
  }
}

export const setOffline = (payloads) => {
  return {
    type: 'OFFLINE',
    payloads
  }
}

export const setOmset = (payloads) => {
  return {
    type: 'OMSET',
    payloads
  }
}

export const setPenarikan = (payloads) => {
  return {
    type: 'PENARIKAN',
    payloads
  }
}