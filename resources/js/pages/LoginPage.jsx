import React, { useEffect, useState } from 'react';
import { appLogin } from '../services/AppService';
// Login Page
export const LoginPage = (props) => {
  const [credentials, setCredentials] = useState({ email: "", password: "" });
  const [state, setState] = useState({ message: "", loading: false, remember_me: false });
  useEffect(() => {
    document.body.className = "hold-transition login-page";
  }, []);
  const signinClicked = () => {
    setState({ message: "", loading: true });
    const resp = appLogin({ email: credentials.email, password: credentials.password, remember_me: state.remember_me });
    resp.promise.then(payloads => {
      console.log(payloads);
      setState({ ...state, loading: false });
    }).catch(err => {
      const message = err.response.data.error ? err.response.data.error :
        err.message ? err.message : "Unknown error";
      setState({ message, loading: false });
    });
  };
  const enterPressed = (event) => {
    if (event.key === 'Enter') {
      signinClicked();
    }
  };
  const inputChanged = (event) => {
    switch (event.target.name) {
      case 'email':
        setCredentials({ ...credentials, email: event.target.value });
        break;
      case 'password':
        setCredentials({ ...credentials, password: event.target.value });
        break;
      case 'remember_me':
        setState({ ...state, remember_me: event.target.checked });
        break;
    }
  };
  return (<div className="login-box">
    <div className="login-logo">
      <a href="#"><small>Dashboard Monitoring<br /><b>e-Tax</b></small></a>
    </div>
    <div className="box login-box-body">
      <p className="login-box-msg">Sign in to start your session</p>
      <form>
        <div className="form-group has-feedback">
          <input type="email" className="form-control" autoComplete="off" id="name" name="email" placeholder="Email" value={credentials.email} onChange={inputChanged} onKeyPress={enterPressed} />
          <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div className="form-group has-feedback">
          <input type="password" className="form-control" id="password" name="password" placeholder="Password" value={credentials.password} onChange={inputChanged} onKeyPress={enterPressed} />
          <span className="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        {state.message && <div className="row">
          <div className="col-xs-12">
            <p className="text-center text-danger">{state.message}</p>
          </div>
        </div>}
        <div className="row">
          <div className="col-xs-8">
            <div className="checkbox">
              <label>
                <input type="checkbox" onChange={inputChanged} /> Remember Me
                </label>
            </div>
          </div>
          <div className="col-xs-4">
            <button type="button" className="btn btn-primary btn-block btn-flat" onClick={signinClicked}>Sign In</button>
          </div>
        </div>
      </form>
      {state.loading && <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>}
    </div>
  </div>);
};
