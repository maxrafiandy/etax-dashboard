import React, { useState, useEffect, useReducer } from 'react';
import { useSelector } from 'react-redux';
import PageAlert from '../components/PageAlert';
import { getProgresOmset, getDetailProgresOmset } from '../services/ReportProgresOmsetService';
import { bulan } from '../master';

const mLocaleString = (value) => {
  return Number(value).toLocaleString("id-ID");
}

const dLocaleString = (value) => {
  return value; // (new Date(value)).toLocaleString("id-ID");
}

const ReportProgresOmset = (props) => {
  const wilayah = useSelector(state => state.auth.wilayah);
  const [alerts, setAlerts] = useState([]);
  const [progres_omset, setProgresOmset] = useState([]);
  const [kode_sudin, setKodeSudin] = useState(wilayah[0].id);
  const [periods, setPeriod] = useState([]);
  const [loading, setLoading] = useState(true);
  const [filter, setFilter] = useState(true);

  useEffect(() => {
    const { promise, source } = getProgresOmset(kode_sudin);
    promise.then(data => {
      setProgresOmset(data.progres_omset);
      setPeriod(data.periods);
      setLoading(false);
      var t = $('#table_progres').DataTable({
        'iDisplayLength': 25,
        "columnDefs": [
          { "type": "html", "targets": [3, 4, 5, 6, 7, 8, 9, 10] }
        ]
      });
      t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
      });

      t.on('order.dt search.dt', function () {
        t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
          cell.innerHTML = i + 1;
        });
      });
    }).catch(err => setAlerts([...alerts, { message: err.message }]));
    return () => source.cancel();
  }, [filter]);

  function filterClicked(e) {
    e.preventDefault();
    setProgresOmset([]);
    setLoading(true);
    setFilter(!filter);
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <h1>Laporan Progres Omset</h1>
        <ol className="breadcrumb">
          <li><a href="#"><i className="fa fa-list-ol"></i> Report</a></li>
          <li className="active">Laporan Progres Omset</li>
        </ol>
      </section>

      <section className="content" style={{ minHeight: 800 }}>
        <div className="row">
          <div className="col-md-12">
            {
              alerts.map((v, i) =>
                <PageAlert type={v.type} title={v.title} message={v.message} icon={v.icon} key={i} />
              )
            }
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="box box-primary">
              <div className="box-header with-border">
                <h3 className="box-title">Progres Omset</h3>
              </div>
              <div className="box-body">
                <div className="col-md-6">
                  <form className="form-horizontal">
                    <div className="box-body">

                      <div className="form-group">
                        <label htmlFor="kode_sudin" className="col-md-3 control-label">WILAYAH</label>
                        <div className="col-md-9">
                          <div className="input-group">
                            <select className="form-control" id="kode_sudin" name="kode_sudin"
                              value={kode_sudin}
                              onChange={(event) => setKodeSudin(event.target.value)}>
                              {
                                wilayah.map((v, i) => <option key={i} value={v.id}>{v.wilayah} ({v.kode})</option>)
                              }
                            </select>
                            <span className="input-group-btn">
                              <button type="button" className="btn btn-default btn-flat" onClick={filterClicked}>FILTER</button>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div className="col-md-12">
                  {
                    !(progres_omset.length > 0) ?
                      <h3 className="text-center" style={{ minHeight: 340 }}>Mengambil data . . .</h3> :
                      <TableProgresOmset progres_omset={progres_omset} periods={periods} />
                  }
                </div>
              </div>
              {loading && <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

const TableProgresOmset = (props) => {
  const [nopd, setNopd] = useState(null);
  return <div className="table-responsive">
    <table id="table_progres" className="table table-bordered table-striped">
      <thead>
        <tr>
          <th className="text-center">NO</th>
          <th colSpan={2} className="text-center">OBJEK PAJAK</th>
          {
            props.periods.map((v, i) => <th colSpan={3} key={i} className="text-center">{bulan[v.long_month]} {v.year}</th>)
          }
          <th colSpan={3} className="text-center">Progres Omset</th>
        </tr>
        <tr>
          <th className="text-center" style={{ width: "10px" }}>#</th>
          <th className="text-center" style={{ width: "100px" }}>NAMA</th>
          <th className="text-center" style={{ width: "80px" }}>NOPD</th>
          <th className="text-center" style={{ width: "20px" }}>TRX</th>
          <th className="text-center">OMS</th>
          <th className="text-center">PJK</th>
          <th className="text-center" style={{ width: "20px" }}>TRX</th>
          <th className="text-center">OMS</th>
          <th className="text-center">PJK</th>
          <th className="text-center" style={{ width: "20px" }}>TRX</th>
          <th className="text-center">OMS</th>
          <th className="text-center">ACT</th>
        </tr>
      </thead>
      <tbody>
        {
          props.progres_omset.map((v, i) => {
            return <tr key={i}>
              <td className="text-center font9"></td>
              <td className="font9">{v.nama_objek_pajak}</td>
              <td className="text-center font9">{v.nopd}</td>
              <td className="text-right font9">{mLocaleString(v.trx1)}</td>
              <td className="text-right font9">{mLocaleString(v.omset1)}</td>
              <td className="text-right font9">{mLocaleString(v.pajak1)}</td>
              <td className="text-right font9">{mLocaleString(v.trx2)}</td>
              <td className="text-right font9">{mLocaleString(v.omset2)}</td>
              <td className="text-right font9">{mLocaleString(v.pajak2)}</td>
              <td className="text-right font9">{mLocaleString(v.trx3)}</td>
              <td className="text-right font9">{mLocaleString(v.omset3)}</td>
              <td className="text-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-default font9" data-toggle="modal"
                    data-target="#modal-default" title="Detail"
                    onClick={() => { setNopd(v.nopd) }}>
                    <i className="fa fa-eye"></i></button>
                </div>
              </td>
            </tr>
          })
        }
      </tbody>
    </table>
    <DetailModal nopd={nopd} />
  </div>
}

const DetailModal = (props) => {
  const iState = { jenis_usaha: "X", detail: [], loading: false };
  const reducer = (state = iState, { type, data }) => {
    switch (type) {
      case 'update': return { jenis_usaha: data.jenis_usaha, detail: data.detail, loading: false };
      case 'set_loading': return { ...state, loading: true };
      case 'unset_loading': return { ...state, loading: false }
    }
  }

  const [state, dispatch] = useReducer(reducer, iState);
  useEffect(() => {
    if (!props.nopd) return;
    dispatch({ type: "set_loading" });
    const { promise, source } = getDetailProgresOmset(props.nopd);
    promise.then(data => dispatch({ type: "update", data }))
      .catch(err => { console.error(err.response); dispatch({ type: "unset_loading" }); });
    return () => source.cancel("Request Cancelled");
  }, [props.nopd]);

  return <div className="modal fade" id="modal-default">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <button type="button"
            onClick={() => dispatch({ type: "update", data: iState })}
            className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 className="modal-title">Detail Transaksi</h4>
        </div>
        <div className="modal-body">
          <h4>NOPD : {state.loading ? "FETCHING DATA . . . " : props.nopd}</h4>
          {state.detail.length > 0 ? <div className="table-responsive">
            {state.jenis_usaha == "H" && <TableFactHiburan data={state.detail} />}
            {state.jenis_usaha == "T" && <TableFactHotel data={state.detail} />}
            {state.jenis_usaha == "R" && <TableFactResto data={state.detail} />}
            {state.jenis_usaha == "P" && <TableFactParkir data={state.detail} />}
          </div> : <h4>LOADING</h4>}
        </div>
        {state.loading && <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>}
      </div>
    </div>
  </div>
}

const TableFactHiburan = (props) => {
  useEffect(() => {
    if (!props.data) return;
    const $el = $("#tbl_detail_hiburan");
    $el.DataTable({
      sort: false,
      columns: [
        { title: "TANGGAL", data: "TANGGAL_TRX", className: "text-center", render: (data) => dLocaleString(data) },
        { title: "RECEIPT", data: "RECEIPTNO" },
        { title: "KAMAR", data: "AMOUNT_KAMAR", className: "text-right", render: (data) => mLocaleString(data) },
        { title: "MAKAN/MINUM", data: "AMOUNT_MAKANMINUM", className: "text-right", render: (data) => mLocaleString(data) },
        { title: "CHARGE", data: "AMOUNT_COVERCHARGE", className: "text-right", render: (data) => mLocaleString(data) },
        { title: "KARCIS", data: "AMOUNT_KARCIS", className: "text-right", render: (data) => mLocaleString(data) },
        { title: "SCORECOIN", data: "AMOUNT_SCORECOIN", className: "text-right", render: (data) => mLocaleString(data) },
        { title: "LAINNYA", data: "AMOUNT_LAINNYA", className: "text-right", render: (data) => mLocaleString(data) }
      ],
      data: props.data
    });
  }, [])
  return <table id="tbl_detail_hiburan" width="100%" className="table table-bordered table-striped"></table>
}

const TableFactHotel = (props) => {
  useEffect(() => {
    if (!props.data) return;
    const $el = $("#tbl_detail_hotel");
    $el.DataTable({
      sort: false,
      columns: [
        { title: "TANGGAL", data: "TANGGAL_TRX", className: "text-center", render: (data) => dLocaleString(data) },
        { title: "RECEIPT", data: "RECEIPT_NO" },
        { title: "DESCRIPTION", data: "DESCRIPTION" },
        { title: "TRANSAKSI", data: "AMOUNT_TRX", className: "text-right", render: (data) => mLocaleString(data) },
      ],
      data: props.data
    });
  }, [])
  return <table id="tbl_detail_hotel" width="100%" className="table table-bordered table-striped"></table>
}

const TableFactResto = (props) => {
  useEffect(() => {
    if (!props.data) return;
    const $el = $("#tbl_detail_resto");
    $el.DataTable({
      sort: false,
      columns: [
        { title: "TANGGAL", data: "TANGGAL_TRX", className: "text-center", render: (data) => dLocaleString(data) },
        { title: "RECEIPT", data: "RECEIPTNO" },
        { title: "TRANSAKSI", data: "AMOUNT_TRX", className: "text-right", render: (data) => mLocaleString(data) },
        { title: "SERVICE", data: "AMOUNT_SERVICE", render: (data) => mLocaleString(data) }
      ],
      data: props.data
    });
  }, [])
  return <table id="tbl_detail_resto" width="100%" className="table table-bordered table-striped"></table>
}

const TableFactParkir = (props) => {
  useEffect(() => {
    if (!props.data) return;
    const $el = $("#tbl_detail_parkir");
    $el.DataTable({
      sort: false,
      columns: [
        { title: "JENIS KENDARAAN", data: "JENIS_KENDARAAN" },
        { title: "NOPOL", data: "NOPOL" },
        { title: "MASUK", data: "WAKTU_MASUK", className: "text-center", render: (data) => dLocaleString(data) },
        { title: "KELUAR", data: "WAKTU_KELUAR", className: "text-center", render: (data) => dLocaleString(data) },
        { title: "TRANSAKSI", data: "AMOUNT_TRX", className: "text-right", render: (data) => mLocaleString(data) }
      ],
      data: props.data
    });
  }, [])
  return <table id="tbl_detail_parkir" width="100%" className="table table-bordered table-striped"></table>
}

export default ReportProgresOmset;