import React, { useEffect, useState } from 'react';
import { useSelector } from  'react-redux';
import PageAlert from '../components/PageAlert';
import { getAutodebet } from '../services/ReportAutodebetService';
import { bulan } from '../master';

const ReportAutodebet = (props) => {
  const wilayah = useSelector(state => state.auth.wilayah);
  const [alerts, setAlerts] = useState([]);
  const [kode_sudin, setKodeSudin] = useState(wilayah[0].id);
  const [status, setStatus] = useState("3");
  const [months, setMonths] = useState([]);
  const [years, setYears] = useState([]);
  const [autodebet, setAutodebet] = useState([]);
  const [loading, setLoading] = useState(true);
  // const [filter, setFilter] = useState(true);

  function load() {
    setLoading(true);
    setAutodebet([]);
    const { promise, source } = getAutodebet(kode_sudin, status);
    promise.then(resp => {
      setAutodebet(resp.autodebet);
      setMonths(resp.months);
      setYears(resp.years);
      if (resp.autodebet.length > 0) {
        var t = $("#tbl_autodebet").DataTable({'iDisplayLength': 25});
        t.column(0, { search: 'applied', order: 'applied' }).nodes().each((cell, i) => {
          cell.innerHTML = i + 1;
        });

        t.on('order.dt search.dt', () => {
          t.column(0, { search: 'applied', order: 'applied' }).nodes().each((cell, i) => {
            cell.innerHTML = i + 1;
          });
        });
      }
      setLoading(false);
    }).catch(err => {
      setAlerts([...alerts, { message: err.message }])
    })
    return () => source.cancel("Request Cancelled");
  }

  function onFilterClicked(event) {
    event.preventDefault();
    setAutodebet([]);
    setLoading(true);
    load();
  }

  useEffect(() => {
    load();
  }, [wilayah]);

  return <div className="content-wrapper">
    <section className="content-header">
      <h1>Laporan Autodebet</h1>
      <ol className="breadcrumb">
        <li><a href="#"><i className="fa fa-list-ol"></i> Report</a></li>
        <li className="active">Laporan Autodebet</li>
      </ol>
    </section>

    <section className="content" style={{ minHeight: 800 }}>
      <Alerts alerts={alerts} />

      <div className="row">
        <div className="col-md-12">
          <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Detail Autodebet</h3>
            </div>
            <div className="box-body">
              <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <form className="form-horizontal">
                  <div className="box-body">
                    <div className="form-group">
                      <label htmlFor="kode_sudin" className="col-md-3 control-label">WILAYAH</label>
                      <div className="col-md-9">
                        <select className="form-control" id="kode_sudin" name="kode_sudin"
                          value={kode_sudin}
                          onChange={(e) => setKodeSudin(e.target.value)}>
                          {wilayah.map((v, i) => <option key={i} value={v.id}>{v.wilayah} ({v.kode})</option>)}
                        </select>
                      </div>
                    </div>

                    <div className="form-group">
                      <label htmlFor="status" className="col-md-3 control-label">STATUS</label>
                      <div className="col-md-9">
                        <div className="input-group">
                          <select className="form-control" id="status" name="status"
                            value={status}
                            onChange={(e) => setStatus(e.target.value)}>
                            <option value={"3"}>BERHASIL</option>
                            <option value={"4"}>GAGAL DEBET</option>
                          </select>
                          <span className="input-group-btn">
                            <button type="button" className="btn btn-default btn-flat" onClick={onFilterClicked}>FILTER</button>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <TableAutodebet autodebet={autodebet} months={months} years={years} loading={loading} />
            </div>
            {loading && <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>}
          </div>
        </div>
      </div>
    </section>
  </div>
}

const Alerts = (props) => {
  return <div className="row">
    <div className="col-md-12">
      {props.alerts.map((v, i) => <PageAlert key={i} type={v.type} title={v.title} message={v.message} icon={v.icon} />)}
    </div>
  </div>
}

const TableAutodebet = (props) => {
  return <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    {!(props.autodebet.length > 0) ?
      <h3 className="text-center" style={{ minHeight: 340 }}>{props.loading ? 'Mengambil data . . . ' :
        <span className="label label-warning">N/A</span>}</h3> :
      <div className="table-responsive">
        <table id="tbl_autodebet" className="table table-bordered table-striped">
          <thead>
            <tr>
              <th className="text-center" style={{ width: "10px" }}>#</th>
              <th className="text-center">Nama Objek Usaha</th>
              {
                props.months.map((v, i) => <th key={i} style={{ width: "20%" }} className="text-center">{bulan[props.months[2 - i]]} {props.years[2 - i]}</th>)
              }
            </tr>
          </thead>
          <tbody>
            {
              props.autodebet.map((v, i) => {
                return <tr key={i}>
                  <td className="text-center"></td>
                  <td className="font9">{v.nama_objek_usaha}</td>
                  <td className="text-right font9">{Number(v.autodebet_1).toLocaleString("id")}</td>
                  <td className="text-right font9">{Number(v.autodebet_2).toLocaleString("id")}</td>
                  <td className="text-right font9">{Number(v.autodebet_3).toLocaleString("id")}</td>
                </tr>
              })
            }
          </tbody>
        </table>
      </div>
    }
  </div>
};

export default ReportAutodebet;