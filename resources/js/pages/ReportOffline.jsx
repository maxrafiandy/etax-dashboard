import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import PageAlert from '../components/PageAlert';
import { getOffline } from '../services/ReportOfflineService';
// import { wilayah } from '../master';

const ReportOffline = (props) => {
  const wilayah = useSelector(state => state.auth.wilayah);
  const [alerts, setAlerts] = useState([]);
  const [log_offline, setLogOffline] = useState([]);
  const [kode_sudin, setKodeSudin] = useState(wilayah[0].id);
  const [loading, setLoading] = useState(true);
  const [filter, setFilter] = useState(true);
  
  useEffect(() => {
    const { promise, source } = getOffline(kode_sudin);
    promise.then(resp => {
      setLogOffline(resp.offline_devices);
      setLoading(false);
      var t = $("#offline_table").DataTable({ 'iDisplayLength': 25 });
      t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
      });

      t.on('order.dt search.dt', function () {
        t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
          cell.innerHTML = i + 1;
        });
      });
    }).catch(err => { setAlerts([...alerts, { message: err.message }]); })

    return () => source.cancel("Request Cancelled");
  }, [filter]);

  const filterClicked = (event) => {
    event.preventDefault();
    setLogOffline([]);
    setLoading(true);
    setFilter(!filter);
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <h1>Laporan Offline</h1>
        <ol className="breadcrumb">
          <li><a href="#"><i className="fa fa-list-ol"></i> Report</a></li>
          <li className="active">Laporan Offline</li>
        </ol>
      </section>

      <section className="content" style={{ minHeight: 800 }}>
        <Alerts alerts={alerts} />
        <div className="row">
          <div className="col-md-12">
            <div className="box box-primary">
              <div className="box-header with-border">
                <h3 className="box-title">Log Perangkat</h3>
              </div>
              <div className="box-body">
                <div className="col-md-6">
                  <form className="form-horizontal">
                    <div className="box-body">
                      <div className="form-group">
                        <label htmlFor="kode_sudin" className="col-md-3 control-label">WILAYAH</label>
                        <div className="col-md-9">
                          <div className="input-group">
                            <select className="form-control" id="kode_sudin" name="kode_sudin"
                              value={kode_sudin}
                              onChange={(event) => setKodeSudin(event.target.value)}>
                              {
                                wilayah.map((v, i) => <option key={i} value={v.id}>{v.wilayah} ({v.kode})</option>)
                              }
                            </select>
                            <span className="input-group-btn">
                              <button type="button" className="btn btn-default btn-flat" onClick={filterClicked}>FILTER</button>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <TableOffline parent={props} log_offline={log_offline} />
              </div>
              {loading && <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

const Alerts = (props) => {
  return <div className="row">
    <div className="col-md-12">
      {props.alerts.map((v, i) =>
        <PageAlert type={v.type} title={v.title}
          message={v.message} icon={v.icon} key={i} />)}
    </div>
  </div>
}

const TableOffline = (props) => {
  return <div className="col-md-12">
    {
      !(props.log_offline.length > 0) ?
        <h3 className="text-center" style={{ minHeight: 340 }}>Mengambil data . . .</h3> :
        <div className="table-responsive">
          <table id="offline_table" className="table table table-bordered table-striped no-margin">
            <thead>
              <tr>
                <th width="10" className="text-center">#</th>
                <th className="text-center">Objek Pajak</th>
                <th className="text-center">Alamat</th>
                <th className="text-center">Last Available</th>
                <th className="text-center">Last Reliable</th>
                <th className="text-center">Keterangan</th>
                <th className="text-center">Detail</th>
              </tr>
            </thead>
            <tbody>
              {
                props.log_offline.map((v, i) => {
                  let last_reliability = v.last_reliable ? v.last_reliable.substring(0, 10) : 'N/A';
                  return <tr key={i}>
                    <td className="text-center font9"></td>
                    <td className="font9">{v.nama_objek_usaha}</td>
                    <td className="font9">{v.alamat}</td>
                    <td className="text-center font9">{v.last_availability ? (new Date(v.last_availability)).toLocaleDateString("id-ID") : <span className="label label-danger">N/A</span>}</td>
                    <td className="text-right font9">{(new Date(last_reliability)).toLocaleDateString("id-ID")}</td>
                    <td className="text-center font9">
                      {v.keterangan ?
                        <span className="label label-success">{v.keterangan}</span> :
                        <span className="label label-warning">Belum ada kunjungan</span>
                      }
                    </td>
                    <td className="text-center">
                      <div className="btn-group">
                        <button type="button" className="btn btn-xs btn-default" data-toggle="tooltip" title="Detail"
                          onClick={() => props.parent.history.push(`/laporan-offline-detail?nopd=${v.nopd}`, { off_device: v })}>
                          <i className="fa fa-search"></i> detail
                        </button>
                      </div>
                    </td>
                  </tr>
                })
              }
            </tbody>
          </table>
        </div>
    }
  </div>
}

export default ReportOffline;