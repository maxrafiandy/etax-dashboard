import React from 'react';
// Footer
export const Footer = () => {
  return (<footer className="main-footer">
    <div className="pull-right hidden-xs">
      <b>Version</b> 1.0.0
        </div>
    <strong>Copyright &copy; 2019 <a href="https://bri.co.id">PT. Bank Rakyat Indonesia (Persero)</a>.</strong> All rights reserved.
    </footer>);
};
