import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { appLogout } from '../services/AppService';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import Dashboard from './Dashboard';
import Availability from './Availability';
import ReportAutodebet from './ReportAutodebet';
import ReportOffline from './ReportOffline';
import ReportOfflineDetail from './ReportOfflineDetail';
import ReportProgresOmset from './ReportProgresOmset';
import ReportPenarikan from './ReportPenarikan';
import Register from './Register';
import UpdatePassword from './UpdatePassword';
import ReportDoubleDebet from './ReportDoubleDebet';
import { Footer } from "./Footer";
import { PageNotFound } from "./PageNotFound";

// Main app component
export const MainPage = (props) => {
  const prefix = "";
  useEffect(() => {
    document.body.className = "hold-transition skin-blue sidebar-mini";
  }, []);
  const logout = () => {
    const resp = appLogout();
    resp.promise.then(resp => {
      localStorage.removeItem("token");
      dispatch(signout());
    });
  };
  return (<Router>

    <div className="modal modal-info fade" id="modal-logout">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h3>Logout</h3>
          </div>
          <div className="modal-body">
            Yakin ingin keluar?
            </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-default" data-dismiss="modal" onClick={logout}>Logout</button>
          </div>
        </div>
      </div>
    </div>

    <div className="wrapper">
      <Header />
      <Sidebar />
      <Switch>
        <Route exact path={prefix + "/"} component={Dashboard} />
        <Route path={prefix + "/availability-perangkat"} component={Availability} />
        <Route path={prefix + "/laporan-autodebet"} component={ReportAutodebet} />
        <Route path={prefix + "/laporan-offline"} component={ReportOffline} />
        <Route path={prefix + "/laporan-offline-detail"} component={ReportOfflineDetail} />
        <Route path={prefix + "/laporan-progres-omset"} component={ReportProgresOmset} />
        <Route path={prefix + "/laporan-penarikan"} component={ReportPenarikan} />
        <Route path={prefix + "/laporan-double-debet"} component={ReportDoubleDebet} />
        <Route path={prefix + "/register"} component={Register} />
        <Route path={prefix + "/update-password"} component={UpdatePassword} />
        <Route component={PageNotFound} />
      </Switch>
      <Footer />
    </div>
  </Router>);
};
