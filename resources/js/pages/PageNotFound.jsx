import React from 'react';
// Page not found
export const PageNotFound = (props) => {
  return (<div id="page_not_found" className="content-wrapper">
    <section className="content-header">
      <h1>Error</h1>
      <ol className="breadcrumb">
        <li><a href="#"><i className="fa fa-dashboard"></i> Errors</a></li>
        <li className="active">404</li>
      </ol>
    </section>
    <section className="content">
      <div className="error-page">
        <h3 className="headline text-yellow"> 404 &nbsp;</h3>
        <div className="error-content">
          <h3><i className="fa fa-warning text-yellow"></i> Oops! Halaman tidak ditemukan.</h3>
          <h4>Halaman yang anda kunjungi tidak dapat ditemukan. Silahkan menggunakan menu sidebar
              sebagai navigasi menggunakan aplikasi ini. Terima kasih</h4>
        </div>
      </div>
    </section>
  </div>);
};
