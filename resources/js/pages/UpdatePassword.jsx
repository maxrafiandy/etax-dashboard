import React, { useState, useEffect } from 'react';
import PageAlert from '../components/PageAlert';
// import details from '../services/UserService';
import putData from '../services/UpdatePasswordService';

const UpdatePassword = (props) => {
  const [alerts, setAlerts] = useState([]);
  const [check_confirm, setCheckConfirm] = useState(false);
  const [old_password, setOldPassword] = useState("");
  const [password, setPassword] = useState("");
  const [c_password, setCPassword] = useState("");

  useEffect(() => {
    
  }, []);

  function update() {
    putData({ old_password, password, c_password })
      .then(resp => setAlerts([...alerts, { message: "Password berhasil terupdate", title: "Sukses!", type: "success", icon: "fa fa-check" }]))
      .catch(err => setAlerts([...alerts, { message: err.message }]))
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <h1>Password</h1>
        <ol className="breadcrumb">
          <li><a href="#"><i className="fa fa-list-ol"></i> Maintenance</a></li>
          <li className="active">Update Password</li>
        </ol>
      </section>

      <section className="content">
        <div className="row">
          <div className="col-md-12">
            {
              alerts.map((v, i) =>
                <PageAlert type={v.type} title={v.title} message={v.message} icon={v.icon} key={i} />
              )
            }
          </div>
        </div>

        <div className="row">
          <div className="col-lg-5 col-md-8 col-sm-12 col-xs-12">
            <div className="box box-primary">

              <div className="box-body">
                {/* <div className="col-lg-5 col-md-8 col-sm-12 col-xs-12"> */}
                  <form>
                    <div className="form-group has-feedback">
                      <label className="control-label">PASSWORD LAMA</label>
                      <input type="password" className="form-control" value={old_password}
                        id="old_password" name="old_password" placeholder="Password Lama"
                        onChange={(e) => setOldPassword(e.target.value)} />
                      <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div className="form-group has-feedback">
                      <label className="control-label">PASSWORD BARU</label>
                      <input type="password" className="form-control" value={password}
                        id="password" name="password" placeholder="Password"
                        onChange={(e) => setPassword(e.target.value)} />
                      <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div className="form-group has-feedback">
                      <label className="control-label">KONFIRMASI PASSWORD BARU</label>
                      <input type="password" className="form-control" value={c_password}
                        id="c_password" name="c_password" placeholder="Ulangi Password"
                        onChange={(e) => setCPassword(e.target.value)} />
                      <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div className="row">
                      <div className="col-xs-8">
                        <div className="checkbox">
                          <label>
                            <input type="checkbox" checked={check_confirm}
                              onChange={(e) => setCheckConfirm(!check_confirm)} /> Konfirmasi?
                          </label>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <button type="button" className="btn btn-primary btn-block btn-flat"
                          disabled={!check_confirm}
                          onClick={update}>UPDATE</button>
                      </div>
                    </div>
                  </form>
                {/* </div> */}
              </div>
            </div>
          </div>
        </div>

      </section>
    </div>
  );
}

export default UpdatePassword;