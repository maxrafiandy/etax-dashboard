import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import PageAlert from '../components/PageAlert';
import { uploadBap, downloadBap, getOfflineHistory } from '../services/ReportOfflineService';

const ReportOfflineDetail = (props) => {
  const [alerts, setAlerts] = useState([]);
  const [keterangan, setKeterangan] = useState("");
  const [source, setSource] = useState(axios.CancelToken.source);
  const [history, setHistory] = useState([]);
  const bap_ref = useRef(null);

  const objek_pajak = props.location.state.off_device;

  useEffect(() => {
    const objek_pajak = props.location.state.off_device 
    const resp = getOfflineHistory(objek_pajak.nopd);
    resp.promise.then(data => { setHistory(data.history) })
      .catch(err => {
        const errors = Object.entries(err.response.data.error)
        if (errors) {
          for (const [type, values] of errors) {
            values.map(value => setAlerts([...alerts, { message: type + ": " + value }]))
          }
        }

        else {
          setAlerts([...alerts, { message: err.message }])
        }
      });
    return () => {
      resp.source.cancel();
      source.cancel();
    };
  }, []);

  function simpanClicked(e) {
    e.preventDefault();
    const { promise, source } = uploadBap({ nopd: objek_pajak.nopd, file: bap_ref.current.files[0], keterangan: keterangan });
    setSource(source);
    promise.then(resp => {
      if (resp)
        setAlerts([...alerts, { title: "Sukses", message: "BA berhasil terupload", icon: "fa fa-check", type: "success" }])
    }).catch(err => {
      const errors = Object.entries(err.response.data.error)
      if (errors) {
        for (const [type, values] of errors) {
          values.map(value => setAlerts([...alerts, { message: type + ": " + value }]))
        }
      }

      else {
        setAlerts([...alerts, { message: err.message }])
      }
    });
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <h1>Laporan Offline <small>Detail</small></h1>
        <ol className="breadcrumb">
          <li><a href="#"><i className="fa fa-list-ol"></i> Report</a></li>
          <li><a href="#"><i className="fa fa-list-ol"></i> Laporan Offline</a></li>
          <li className="active">Detail</li>
        </ol>
      </section>

      <section className="content" style={{ minHeight: 800 }}>
        <div className="row">
          <div className="col-md-12">
            {
              alerts.map((v, i) =>
                <PageAlert type={v.type} title={v.title} message={v.message} icon={v.icon} key={i} />
              )
            }
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <div className="box box-solid">
              <div className="box-header with-border">
                <button className="btn btn-xs" style={{ marginTop: 5, marginBottom: 5 }}
                  onClick={() => props.history.goBack()}><i className="fa fa-chevron-left"></i></button>
                &nbsp; &nbsp;
                <i className="fa fa-text-width"></i>
                <h3 className="box-title">Details</h3>
              </div>

              <div className="box-body">
                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <dl className="dl-horizontal">
                    <dt>Nama Objek Usaha</dt>
                    <dd>{objek_pajak.nama_objek_usaha ? objek_pajak.nama_objek_usaha : 'N/A'}</dd>

                    <dt>Alamat</dt>
                    <dd>{objek_pajak.alamat ? objek_pajak.alamat : 'N/A'}</dd>

                    <dt>NOPD</dt>
                    <dd>{objek_pajak.nopd ? objek_pajak.nopd : 'N/A'}</dd>

                    <dt>Nama Wajib Pajak</dt>
                    <dd>{objek_pajak.nama_wajib_pajak ? objek_pajak.nama_wajib_pajak : 'N/A'}</dd>

                    <dt>NPWPD</dt>
                    <dd>{objek_pajak.npwpd ? objek_pajak.npwpd : 'N/A'}</dd>

                    <dt>Jenis Usaha</dt>
                    <dd>{objek_pajak.jenis_usaha ? objek_pajak.jenis_usaha : 'N/A'}</dd>

                    <dt>Jenis Penarikan</dt>
                    <dd>{objek_pajak.jenis_penarikan ? objek_pajak.jenis_penarikan : 'N/A'}</dd>

                    <dt>IP Address</dt>
                    <dd>{objek_pajak.ipaddress ? objek_pajak.ipaddress : 'N/A'}</dd>

                    <dt>Online Terakhir</dt>
                    <dd>{objek_pajak.last_availability ? objek_pajak.last_availability : 'N/A'}</dd>

                    <dt>Reliable Terakhir</dt>
                    <dd>{objek_pajak.last_reliable ? objek_pajak.last_reliable.substring(0, 10) : 'N/A'}</dd>

                    <dt>Status</dt>
                    <dd>{objek_pajak.status ? objek_pajak.status : 'N/A'}</dd>

                  </dl>
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <TableHistoryOffline history={history}></TableHistoryOffline>
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <dt>Berita Acara Kunjungan</dt>
                  <dd>
                    <input type="file" style={{ marginTop: 3 }} accept=".pdf"
                      ref={bap_ref} />
                  </dd>
                  <dt>Keterangan</dt>
                  <dd>
                    <textarea className="form-control" rows="3" value={keterangan} onChange={e => setKeterangan(e.target.value)}
                      placeholder="Deskripsi keterangan kunjungan . . ."></textarea>
                    {/* <input type="text" value={keterangan} onChange={e => setKeterangan(e.target.value)} /> */}
                  </dd>
                  <dd>
                    <button type="button" className="btn btn-success pull-right" style={{ margin: 3 }}
                      onClick={simpanClicked}><i className="fa fa-upload"></i> UNGGAH</button>
                  </dd>
                </div>
                <div className="col-md-12">
                  <button className="btn btn-sm" style={{ marginTop: 5, marginBottom: 5 }}
                    onClick={() => props.history.goBack()}><i className="fa fa-chevron-left"></i> Kembali</button>
                </div>
                <div className="clear-fix"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

const TableHistoryOffline = (props) => {
  const { history } = props;

  const download = (id) => {
    event.preventDefault();
    const { promise, source } = downloadBap(id);
    var fileDownload = require('js-file-download');
    promise.then(data => { fileDownload(data, 'berita_acara.pdf') })
    // return () => source.cancel();
  }

  return <table id="tbl_history" width="100%" className="table table table-bordered table-striped">
    <thead>
      <tr>
        <th className="text-center">#</th>
        <th className="text-center">Tanggal Kunjungan</th>
        <th className="text-center">Berita Acara</th>
        <th className="text-center">Keterangan</th>
        <th className="text-center">Action</th>
      </tr>
    </thead>
    <tbody>
      {history.map((h, i) => <tr key={i}>
        <td className="text-center">{i + 1}</td>
        <td>{new Date(h.created_at).toLocaleString("id")}</td>
        <td>{h.attachment}</td>
        <td className="text-center">{h.keterangan}</td>
        <td className="text-center">
          <div className="btn-group">
            <button type="button" className="btn btn-xs btn-primary" data-toggle="tooltip" title="Download"
              onClick={() => download(h.id)}>
              <i className="fa fa-download"></i> UNDUH
            </button>
            {/* <a href={"api/offline-devices/bap/download/" + h.id} className="btn btn-xs btn-primary" download>
              <i className="fa fa-download"></i> UNDUH
            </a> */}
          </div>
        </td>
      </tr>)}
    </tbody>
  </table>
}

export default ReportOfflineDetail;