import React, { Component } from 'react';
import PageAlert from '../components/PageAlert';

export default class Example extends Component {

  constructor(props) {
    super(props);
    this.state = {
      alerts: []
    };
  }

  render() {

    return (
      <div id="example">
        <section className="content-header">
          <h1>Example</h1>
          <ol className="breadcrumb">
            <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
            <li className="active">Example</li>
          </ol>
        </section>

        <section className="content">
          <div className="row">
            <div className="col-md-12">
              {
                this.state.alerts.map((v, i) =>
                  <PageAlert type={v.type} title={v.title} message={v.message} icon={v.icon} key={i} />
                )
              }
            </div>
          </div>

        </section>
      </div>
    );
  }
}
