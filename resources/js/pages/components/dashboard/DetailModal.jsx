const DetailModal = (props) => {

    useEffect(() => {
      const $el = $("#table_detail");
      var t = $el.DataTable({
        columns: [
          { title: "#", data: null },
          { title: "NOPD", data: "nopd" },
          { title: "Nama Objek Pajak", data: "nama_objek_usaha" },
          // { title: "Alamat", data: "alamat" },
          { title: "Online Terakhir", data: "last_status" },
          {
            title: "Status", data: function (row, type, val, meta) {
              // console.log(row);
              return row.status == 0 ? "Offline" : "Online"
            }
          }
        ]
      });
      t.on('order.dt search.dt', function () {
        t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
          cell.innerHTML = i + 1;
        });
      });
    }, []);
  
    useEffect(() => {
      if (!props.kode_sudin) return;
      const $el = $("#table_detail");
      $el.dataTable().fnClearTable();
      const { promise, source } = getOpSudin(props.kode_sudin);
      promise.then(data => {
        $el.dataTable().fnAddData(data);
      }).catch(err => console.error(err.response));
      return () => {
        source.cancel("Request Cancelled");
      }
    }, [props.kode_sudin]);
  
    return <div className="modal fade" id="modal-default">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 className="modal-title">{props.title}</h4>
          </div>
          <div className="modal-body">
            <div className="table-responsive">
              <table id="table_detail" width="100%" className="table table-bordered table-striped"></table>
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-default"
              data-dismiss="modal">Tutup</button>
          </div>
        </div>
      </div>
    </div>
  }