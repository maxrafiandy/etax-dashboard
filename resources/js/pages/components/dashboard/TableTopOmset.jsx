const TableTopOmset = (props) => {
    const [state, setState] = useState({ last_transaction: [] });
  
    useEffect(() => {
      function fetch() {
        const { promise, source } = getTopOmset();
        promise.then(data => {
          setState({ last_transaction: data });
        }).catch(err => console.error(err.response));
        return () => {
          source.cancel("Request Cancelled");
        }
      }
  
      fetch();
    }, []);
  
    return <div className="box box-info">
      <div className="box-header with-border">
        <h3 className="box-title">Top Omset <small>Bulan Berjalan</small></h3>
      </div>
      <div className="box-body">
        <table id="table_transaction" className="table table-responsive table-bordered table-striped">
          <thead>
            <tr>
              <th className="text-center">#</th><th className="text-center">OP</th><th className="text-center">OMS</th>
            </tr>
          </thead>
          <tbody>
            {state.last_transaction.map((t, i) => <tr key={i}>
              <td className="font9">{i + 1}</td>
              <td className="font9">{t.nama_objek_pajak}</td>
              <td className="font9">{Number(t.omset).toLocaleString("id")}</td>
            </tr>)}
          </tbody>
        </table>
      </div>
    </div>
  }

  export default TableTopOmset;