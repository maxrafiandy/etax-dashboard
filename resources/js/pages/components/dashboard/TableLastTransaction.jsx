const TableLastTransaction = (props) => {
    const [state, setState] = useState({ last_transaction: [] });
  
    useEffect(() => {
      function fetch() {
        const { promise, source } = getLastTransaction();
        promise.then(data => {
          setState({ last_transaction: data })
        }).catch(err => console.error(err.response));
        return () => {
          source.cancel("Request Cancelled");
        }
      }
  
      fetch();
    }, []);
  
    return <div className="box box-info">
      <div className="box-header with-border">
        <h3 className="box-title">Transaksi <small>Terakhir</small></h3>
      </div>
      <div className="box-body">
        <table id="table_transaction" className="table table-responsive table-bordered table-striped">
          <thead>
            <tr>
              <th className="text-center">#</th><th className="text-center">OP</th><th className="text-center">TRX</th>
            </tr>
          </thead>
          <tbody>
            {state.last_transaction.map((t, i) => <tr key={i}>
              <td className="font9">{i + 1}</td>
              <td className="font9">{t.nama_objek_usaha}</td>
              <td className="font9">{new Date(t.last_data).toLocaleString("id")}</td>
            </tr>)}
          </tbody>
        </table>
      </div>
    </div>
  }

  export default TableLastTransaction;