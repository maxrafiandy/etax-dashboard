const DashboardBox = (props) => {
    return <div className="col-md-3 col-sm-6 col-xs-12">
      <div className="info-box">
        <span className={`info-box-icon ${props.bg ? props.bg : "bg-aqua"}`}>
          <i className={props.icon ? props.icon : "fa fa-coffee"}></i>
        </span>
        <div className="info-box-content">
          <span className="info-box-text">{props.title}</span>
          <span className="info-box-number">{props.value}</span>
          <span className="info-box-text">{props.percentage}%</span>
        </div>
      </div>
    </div>
  }

  export default DashboardBox;