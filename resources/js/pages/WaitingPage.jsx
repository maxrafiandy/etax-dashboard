import React, { useEffect } from 'react';
// Waiting Page
export const WaitingPage = () => {
  useEffect(() => {
    document.body.className = "hold-transition skin-blue sidebar-mini";
  }, []);
  return <div className="login-box">
    <div className="login-logo">
      <a href="#"><small><b>L O A D I N G</b></small></a>
      <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>
    </div>
  </div>;
};
