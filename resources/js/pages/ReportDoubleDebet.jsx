import React, { useEffect } from 'react';
import PageAlert from '../components/PageAlert';
import { useDispatch, useSelector } from 'react-redux';
import DoubleDebetService from '../services/ReportDoubleDebetService';

const ReportDoubleDebet = () => {
  const dispatch = useDispatch();
  const self = useSelector(state => state.double_debet);

  // on page load
  useEffect(() => {
    var t;
    const { promise, source } = DoubleDebetService.doubleDebet();
    promise.then(resp => {
      dispatch({ type: 'DDEBET_SET_DATA', payloads: { double_debet: resp.data.double_debet } });
    })
      .then(() => {
        t = $("#tbl_doubledebet").DataTable({ 'iDisplayLength': 10 });
        t.column(0, { search: 'applied', order: 'applied' }).nodes().each((cell, i) => {
          cell.innerHTML = i + 1;
        });

        t.on('order.dt search.dt', () => {
          t.column(0, { search: 'applied', order: 'applied' }).nodes().each((cell, i) => {
            cell.innerHTML = i + 1;
          });
        });
      })
      .catch(err => {
        dispatch({ type: 'DDEBET_UNSET_LOADING' });
        dispatch(dispatch({ type: 'DDEBET_SET_ALERTS', payloads: { alerts: [...self.alerts, { message: err.message }] } }))
      })
    return () => {
      t.destroy();
      source.cancel("Double Debet Closed")
    }
  }, []);

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <h1>Laporan Double Debet</h1>
        <ol className="breadcrumb">
          <li><a href="#"><i className="fa fa-list-ol"></i> Report</a></li>
          <li className="active">Laporan Double Debet {self.double_debet.length}</li>
        </ol>
      </section>

      <section className="content" style={{ minHeight: 800 }}>
        <div className="row">
          <div className="col-md-12">
            {
              self.alerts.map((v, i) =>
                <PageAlert type={v.type} title={v.title} message={v.message} icon={v.icon} key={i} />
              )
            }
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="box box-primary">
              <div className="box-header with-border">
                <h3 className="box-title">Double Debet</h3>
              </div>
              <div className="box-body">
                <table id="tbl_doubledebet" className="table table table-bordered table-striped no-margin">
                  <thead>
                    <tr>
                      <th width="10" className="text-center">#</th>
                      <th className="text-center">NOPD</th>
                      <th className="text-center">OBJEK USAHA</th>
                      <th className="text-center">PAJAK</th>
                      <th className="text-center">JML DEBET</th>
                      <th className="text-center">TOTAL DEBET</th>
                      <th className="text-center">JML REVERSAL</th>
                      <th className="text-center">TOTAL REVERSAL</th>
                      <th className="text-center">DETAIL</th>
                    </tr>
                  </thead>
                  {self.double_debet.length > 0 ? <tbody> {
                    self.double_debet.map((v, i) => {
                      return <tr key={i}>
                        <td className="text-center font9"></td>
                        <td className="font9">{v.nopd}</td>
                        <td className="font9">{v.nama_objek_usaha}</td>
                        <td className="text-right font9">{v.pajak}</td>
                        <td className="text-right font9">{v.jml_debet}</td>
                        <td className="text-right font9">{v.total_debet}</td>
                        <td className="text-right font9">{v.jml_reversal}</td>
                        <td className="text-right font9">{v.total_reversal}</td>
                        <td className="text-center font9">{v.keterangan}</td>
                      </tr>
                    })}
                  </tbody> : <tbody></tbody>}
                </table>
              </div>
              {self.loading && <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default ReportDoubleDebet;