import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import format from 'format-number';
import PageAlert from '../components/PageAlert';
import { bulan } from '../master';
import { getPenarikan } from '../services/ReportPenarikanService';

const f = format;
const n = Number;

const ReportPenarikan = (props) => {
  const [alerts, setAlerts] = useState([]);
  const [penarikan, setPenarikan] = useState([]);
  const [kode_sudin, setKodeSudin] = useState("01");
  const [loading, setLoading] = useState(true);
  const [month, setMonth] = useState("00");
  const [year, setYear] = useState("2019");
  const [filter, setFilter] = useState(false);
  const wilayah = useSelector(state => state.auth.wilayah);

  useEffect(() => {
    const { promise, source } = getPenarikan(kode_sudin);
    promise.then(data => {
      setPenarikan(data.penarikan);
      setMonth(data.month);
      setYear(data.year);
      setLoading(false);
      var t = $('#table_penarikan').DataTable({'iDisplayLength': 25});
      t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
      });

      t.on('order.dt search.dt', function () {
        t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
          cell.innerHTML = i + 1;
        });
      });
    }).catch(err => setAlerts([...alerts, { message: err.message }]))
    return () => source.cancel();
  }, [filter]);

  function filterClicked(e) {
    e.preventDefault();
    setPenarikan([]);
    setLoading(true);
    setFilter(!filter);
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <h1>Laporan Penarikan</h1>
        <ol className="breadcrumb">
          <li><a href="#"><i className="fa fa-list-ol"></i> Report</a></li>
          <li className="active">Laporan Penarikan</li>
        </ol>
      </section>

      <section className="content" style={{ minHeight: 800 }}>
        <Alerts alerts={alerts} />
        <div className="row">
          <div className="col-md-12">
            <div className="box box-primary">
              <div className="box-header with-border">
                <h3 className="box-title">Penarikan</h3>
              </div>
              <div className="box-body">
                <div className="col-md-6">
                  <form className="form-horizontal">
                    <div className="box-body">

                      <div className="form-group">
                        <label htmlFor="kode_sudin" className="col-md-3 control-label">WILAYAH</label>
                        <div className="col-md-9">
                          <div className="input-group">
                            <select className="form-control" id="kode_sudin" name="kode_sudin"
                              value={kode_sudin}
                              onChange={(event) => setKodeSudin(event.target.value)}>
                              {
                                wilayah.map((v, i) => <option key={i} value={v.id}>{v.wilayah} ({v.kode})</option>)
                              }
                            </select>
                            <span className="input-group-btn">
                              <button type="button" className="btn btn-default btn-flat" onClick={filterClicked}>FILTER</button>
                            </span>
                          </div>
                        </div>
                      </div>

                    </div>
                  </form>
                </div>

                <div className="col-md-12">
                  {
                    !(penarikan.length > 0) ?
                      <h3 className="text-center" style={{ minHeight: 340 }}>Mengambil data . . .</h3> :
                      <TablePenarikan penarikan={penarikan} month={bulan[month]} year={year} />
                  }
                </div>
              </div>

              {loading && <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

const TablePenarikan = (props) => {
  return <div className="table table-responsive">
    <table id="table_penarikan" className="table table table-bordered table-striped no-margin">
      <thead>
        <tr>
          <th rowSpan={2} className="text-center align-center" style={{ width: 10 }}>#</th>
          <th rowSpan={2} className="text-center" style={{ width: 100 }}>Objek Pajak</th>
          <th rowSpan={2} className="text-center">NOPD</th>
          <th colSpan={2} className="text-center">Penarikan BRI<br />({props.month} {props.year})</th>
          <th colSpan={2} className="text-center">Adjustment<br />({props.month} {props.year})</th>
          <th colSpan={3} className="text-center">Autodebet Pajak<br />({props.month} {props.year})</th>
        </tr>
        <tr>
          <th className="text-center">TRX</th>
          <th className="text-center">JML</th>
          <th className="text-center">TRX</th>
          <th className="text-center">JML</th>
          <th className="text-center">TRX</th>
          <th className="text-center">OMS</th>
          <th className="text-center">SETOR</th>
        </tr>
      </thead>
      <tbody>
        {
          props.penarikan.map((v, i) => {
            const penarikan_bri = Number(v.trx1) - Number(v.trx2);
            const omset_bri = Number(v.omset1) - Number(v.omset2);

            // if (omset_bri != 0 && Number(v.omset2) != 0 && Number(v.omset3) != 0)
            return <tr key={i}>
              <td className="text-center font9"></td>
              <td className="font9">{v.nama_objek_pajak}</td>
              <td className="text-center font9">{v.nopd}</td>
              <td className="text-right font9">{Number(penarikan_bri < 1 ? penarikan_bri : 0).toLocaleString("id")}</td>
              <td className="text-right font9">{Number(omset_bri).toLocaleString("id")}</td>
              <td className="text-right font9">{Number(v.trx2).toLocaleString("id")}</td>
              <td className="text-right font9">{Number(v.omset2).toLocaleString("id")}</td>
              <td className="text-right font9">{Number(v.trx3).toLocaleString("id")}</td>
              <td className="text-right font9">{Number(v.omset3).toLocaleString("id")}</td>
              <td className="text-right font9">{Number(v.setoran).toLocaleString("id")}</td>
            </tr>
          })
        }
      </tbody>
    </table>
  </div>
}

const Alerts = (props) => {
  return <div className="row">
    <div className="col-md-12">
      {
        props.alerts.map((v, i) =>
          <PageAlert type={v.type} title={v.title} message={v.message} icon={v.icon} key={i} />
        )
      }
    </div>
  </div>
}
export default ReportPenarikan;