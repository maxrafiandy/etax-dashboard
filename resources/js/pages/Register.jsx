import React, { useState, useEffect, useReducer } from 'react';
import { useSelector } from 'react-redux';
import PageAlert from '../components/PageAlert';
import { postData } from '../services/RegisterService';
import { suban } from '../master';

const iState = { name: '', email: '', password: '', c_password: '', level: 'USER', suban: 0, check_confirm: false };
const reducer = (state = iState, { type, data }) => {
  switch (type) {
    case 'name': return { ...state, name: data };
    case 'email': return { ...state, email: data };
    case 'password': return { ...state, password: data };
    case 'c_password': return { ...state, c_password: data };
    case 'level': return { ...state, level: data };
    case 'suban': return { ...state, suban: data}
    case 'check_confirm': return { ...state, check_confirm: data };
    case 'reset': return iState;
  }
}

const Register = (props) => {
  const [alerts, setAlerts] = useState([]);
  const [state, dispatch] = useReducer(reducer, iState);

  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    if (auth.level !== "ADMIN") props.history.replace("/");
  }, []);

  function post() {
    const user = {
      email: state.email, name: state.email,
      password: state.password, c_password: state.password,
      level: state.level, suban: state.suban
    };

    postData(user)
      .then(resp => {
        setAlerts([...alerts, {
          title: "Sukses",
          message: "1 (satu) user berhasil ditambahkan",
          icon: "fa fa-check", type: "success"
        }]);
        dispatch({ type: 'reset' });
      })
      .catch(err => {
        if (err.response.data.error) {
          const errors = Object.entries(err.response.data.error)
          for (const [type, values] of errors) {
            values.map(value => setAlerts([...alerts, { message: type + ": " + value }]))
          }
        }

        else {
          setAlerts([...alerts, { message: err.message }])
        }
      });
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <h1>Register</h1>
        <ol className="breadcrumb">
          <li><a href="#"><i className="fa fa-list-ol"></i> Maintenance</a></li>
          <li className="active">Register</li>
        </ol>
      </section>

      <section className="content">
        <div className="row">
          <div className="col-md-12">
            {
              alerts.map((v, i) =>
                <PageAlert type={v.type} title={v.title} message={v.message} icon={v.icon} key={i} />
              )
            }
          </div>
        </div>
        <div className="row">
          <div className="col-lg-5 col-md-8 col-sm-12 col-xs-12">
            <div className="box box-primary">
              <div className="box-header with-border">
                <h3 className="box-title">Data Login User Baru</h3>
              </div>
              <div className="box-body">
                <form>
                  <div className="form-group has-feedback">
                    <label className="control-label">Full Name</label>
                    <input type="text" className="form-control" value={state.name}
                      id="name" name="name" placeholder="Username"
                      onChange={(e) => dispatch({ type: 'name', data: e.target.value })} />
                    <span className="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                  <div className="form-group has-feedback">
                    <label className="control-label">EMAIL</label>
                    <input type="email" className="form-control" value={state.email}
                      id="email" name="email" placeholder="Email"
                      onChange={(e) => dispatch({ type: 'email', data: e.target.value })} />
                    <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
                  <div className="form-group has-feedback">
                    <label className="control-label">PASSWORD</label>
                    <input type="password" className="form-control" value={state.password}
                      id="password" name="password" placeholder="Password"
                      onChange={(e) => dispatch({ type: 'password', data: e.target.value })} />
                    <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div className="form-group has-feedback">
                    <label className="control-label">KONFIRMASI PASSWORD</label>
                    <input type="password" className="form-control" value={state.c_password}
                      id="c_password" name="c_password" placeholder="Ulangi Password"
                      onChange={(e) => dispatch({ type: 'c_password', data: e.target.value })} />
                    <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>

                  <div className="form-group has-feedback">
                    <label className="control-label">LEVEL ADMINISTRASI</label>
                    <select className="form-control" value={state.level}
                      id="level" name="level"
                      onChange={(e) => dispatch({ type: 'level', data: e.target.value })}>
                      <option value={"USER"}>USER</option>
                      <option value={"ADMIN"}>ADMIN</option>
                    </select>
                  </div>

                  <div className="form-group has-feedback">
                    <label className="control-label">WILAYAH/SUBAN</label>
                    <select className="form-control" value={state.suban}
                      id="suban" name="suban"
                      onChange={(e) => dispatch({ type: 'suban', data: e.target.value })}>
                      <option value={0}>BPRD</option>
                      <option value={1}>JAKARTA PUSAT</option>
                      <option value={3}>JAKARTA SELATAN</option>
                      <option value={5}>JAKARTA TIMUR</option>
                      <option value={7}>JAKARTA BARAT</option>
                      <option value={9}>JAKARTA UTARA</option>
                    </select>
                  </div>

                  <div className="row">
                    <div className="col-xs-8">
                      <div className="checkbox">
                        <label>
                          <input type="checkbox" checked={state.check_confirm}
                            onChange={() => dispatch({
                              type: 'check_confirm',
                              data: !state.check_confirm
                            })} /> Seluruh data benar?
                        </label>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <button type="button" className="btn btn-primary btn-block btn-flat"
                        disabled={!state.check_confirm} onClick={post}>REGISTER</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Register;