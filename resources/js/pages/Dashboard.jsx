import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import format from 'format-number';
import { Chart } from 'chart.js';
import {
  getOpSudin,
  // v2
  omsetJenisUsaha, opJenisUsaha, omsetSudin, topAutodebet, topOmset, statusPerSudin
} from '../services/DashboardService';

const n = Number;
const f = format;

const Dashboard = (props) => {
  const [kode_sudin, setKodeSudin] = useState("");
  const [modal_title, setModalTitle] = useState("Sudin Pusat");

  useEffect(() => {
    const source = statusPerSudin();
    return () => source.cancel("Request Cancelled");
  }, []);

  const dashboard = useSelector(state => state.dashboard);

  return <div className="content-wrapper">
    <section className="content-header">
      <h1>Dashboard</h1>
      <ol className="breadcrumb">
        <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
        <li className="active">Dashboard</li>
      </ol>
    </section>

    <section className="content" style={{ minHeight: 340 }}>
      <div className="row">
        <DashboardBox bg="bg-aqua" title="Restoran"
          kode_jenis_usaha="R" icon="fa fa-coffee" omset={dashboard.omset_resto}
          total_op={dashboard.op_resto} />
        <DashboardBox bg="bg-red" title="Parkir"
          kode_jenis_usaha="P" icon="fa fa-car" omset={dashboard.omset_parkir}
          total_op={dashboard.op_parkir} />
        <div className="clearfix visible-sm-block"></div>
        <DashboardBox bg="bg-green" title="Hiburan"
          kode_jenis_usaha="H" icon="fa fa-music" omset={dashboard.omset_hiburan}
          total_op={dashboard.op_hiburan} />
        <DashboardBox bg="bg-yellow" title="Hotel"
          kode_jenis_usaha="T" icon="fa fa-hotel" omset={dashboard.omset_hotel}
          total_op={dashboard.op_hotel} />

        <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
          <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Sebaran <small>Perangkat</small></h3>
            </div>
            <div className="box-body">
              <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 vertical-center">
                <PieChart />
                <p className="text-center">
                  <small className="label label-info">{n(dashboard.op_online).toLocaleString("id")}</small>&nbsp;
                    <small className="label label-warning">{n(dashboard.op_offline).toLocaleString("id")}</small>
                </p>
              </div>
              <TableSebaranPerangkat data={dashboard.status_per_sudin} setKodeSudin={setKodeSudin} setModalTitle={setModalTitle} />
            </div>
          </div>
        </div>

        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <TableOmsetSudin />
        </div>

        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <TableTopAutodebet />
        </div>

        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <TableTopOmset />
        </div>

      </div>
    </section>
    <DetailModal title={modal_title} kode_sudin={kode_sudin} loading={true} />
  </div>
}

const TableSebaranPerangkat = (props) => {
  return <div className="table-responsive">
    <table className="table table-bordered table-striped">
      <thead>
        <tr>
          <th width="10">#</th>
          <th className="text-center font9">Wilayah</th>
          <th className="text-center font9">OP Offline</th>
          <th className="text-center font9">OP Online</th>
          <th className="text-center font9">Persentase Online</th>
          <th className="text-center font9">Action</th>
        </tr>
      </thead>
      <tbody>
        {
          props.data.length > 0 ? props.data.map((v, i) =>
            <tr key={i}>
              <td className="font9">{i + 1}</td>
              <td className="font9">{v.nama_sudin}</td>
              <td className="text-center font9">{f({})(v.jumlah_offline)}</td>
              <td className="text-center font9">{f({})(v.jumlah_online)}</td>
              <td className="text-center font9">{f({ suffix: '%', round: 1 })
                (n(v.jumlah_online) * 100 / (n(v.jumlah_online) + n(v.jumlah_offline)))}
              </td>
              <td className="text-center font9">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-default font9" data-toggle="modal"
                    data-target="#modal-default" title="Detail"
                    onClick={() => { props.setKodeSudin(v.kode_sudin); props.setModalTitle(v.nama_sudin) }}>
                    <i className="fa fa-eye"></i>
                  </button>
                </div>
              </td>
            </tr>
          ) : <tr><td className="font9 text-center" colSpan="6" rowSpan="5">BELUM ADA DATA</td></tr>
        }
      </tbody>
    </table>
  </div>
}

const PieChart = () => {
  const dashboard = useSelector(state => state.dashboard);
  useEffect(() => {
    const pie = new Chart($('#pie'), {
      type: 'pie',
      data: {
        datasets: [{
          data: [dashboard.op_online, dashboard.op_offline],
          backgroundColor: [Color.info, Color.warning],
          label: 'Perbandingan Online dan Offline'
        }],
        labels: ['online', 'offline']
      },
      options: { responsive: true }
    })
    pie.render();
  }, [dashboard.op_online]);
  return <canvas id="pie" height="200%"></canvas>
}

const DashboardBox = (props) => {

  useEffect(() => {
    const source = omsetJenisUsaha(props.kode_jenis_usaha);
    return () => source.cancel("request cancelled");
  }, []);

  useEffect(() => {
    const source = opJenisUsaha(props.kode_jenis_usaha);
    return () => source.cancel("request cancelled");
  }, []);

  return <div className="col-md-3 col-sm-6 col-xs-12">
    <div className="info-box">
      <span className={`info-box-icon ${props.bg ? props.bg : "bg-aqua"}`}>
        <i className={props.icon ? props.icon : "fa fa-coffee"}></i>
      </span>
      <div className="info-box-content">
        <span className="info-box-text font10">OMSET {props.title}</span>
        <span className="info-box-number font12">RP{Number(props.omset).toLocaleString("id")}</span>
        <span className="info-box-text font11">TOTAL OP : {Number(props.total_op).toLocaleString("id")}</span>
      </div>
    </div>
  </div>
}

const DetailModal = (props) => {

  useEffect(() => {
    const $el = $("#table_detail");
    var t = $el.DataTable({
      columns: [
        { title: "#", data: null },
        { title: "NOPD", data: "nopd" },
        { title: "Nama Objek Pajak", data: "nama_objek_usaha" },
        { title: "Online Terakhir", data: "last_status" },
        { title: "Status", data: (row) => row.status == 0 ? "OFF" : "ON" }
      ]
    });
    t.on('order.dt search.dt', () => {
      t.column(0, { search: 'applied', order: 'applied' }).nodes().each((cell, i) => {
        cell.innerHTML = i + 1;
      });
    });
  }, []);

  useEffect(() => {
    if (!props.kode_sudin) return;
    const $el = $("#table_detail");
    $el.dataTable().fnClearTable();
    const { promise, source } = getOpSudin(props.kode_sudin);
    promise.then(data => {
      $el.dataTable().fnAddData(data);
    }).catch(err => console.error(err.response));
    return () => source.cancel("Request Cancelled");
  }, [props.kode_sudin]);

  return <div className="modal fade" id="modal-default">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 className="modal-title">{props.title}</h4>
        </div>
        <div className="modal-body">
          <div className="table-responsive">
            <table id="table_detail" width="100%" className="table table-bordered table-striped"></table>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default"
            data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
}

const TableTopAutodebet = (props) => {
  useEffect(() => {
    const source = topAutodebet();
    return () => source.cancel("Request Cancelled");
  }, []);

  const dashboard = useSelector(state => state.dashboard);

  return <div className="box box-info">
    <div className="box-header with-border">
      <h3 className="box-title">Top Autodebet <small>Bulan Lalu</small></h3>
    </div>
    <div className="box-body">
      <table id="table_transaction" className="table table-responsive table-bordered table-striped">
        <thead>
          <tr>
            <th className="text-center">#</th><th className="text-center">OP</th><th className="text-center">JML</th>
          </tr>
        </thead>
        <tbody>
          {dashboard.top_autodebet.length > 0 ? dashboard.top_autodebet.map((t, i) => <tr key={i}>
            <td className="font9">{i + 1}</td>
            <td className="font9">{t.nama_objek_usaha}</td>
            <td className="font9">{Number(t.autodebet).toLocaleString("id")}</td>
          </tr>) : <tr><td className="font9 text-center" colSpan="3"  rowSpan="5">BELUM ADA DATA</td></tr>}
        </tbody>
      </table>
    </div>
  </div>
}

const TableTopOmset = (props) => {

  useEffect(() => {
    const source = topOmset();
    return () => source.cancel("Request Cancelled");
  }, []);

  const dashboard = useSelector(state => state.dashboard);

  return <div className="box box-info">
    <div className="box-header with-border">
      <h3 className="box-title">Top Omset <small>Bulan Berjalan</small></h3>
    </div>
    <div className="box-body">
      <table id="table_transaction" className="table table-responsive table-bordered table-striped">
        <thead>
          <tr><th className="text-center">#</th><th className="text-center">OP</th><th className="text-center">OMS</th></tr>
        </thead>
        <tbody>
          {dashboard.top_omset.length > 0 ? dashboard.top_omset.map((t, i) => <tr key={i}>
            <td className="font9">{i + 1}</td>
            <td className="font9">{t.nama_objek_pajak}</td>
            <td className="font9">{Number(t.omset).toLocaleString("id")}</td>
          </tr>) : <tr><td className="font9 text-center" colSpan="3" rowSpan="5">BELUM ADA DATA</td></tr>}
        </tbody>
      </table>
    </div>
  </div>
}

const TableOmsetSudin = (props) => {

  useEffect(() => {
    const source = omsetSudin();
    return () => source.cancel("Request Cancelled");
  }, []);

  const dashboard = useSelector(state => state.dashboard);

  return <div className="box box-info">
    <div className="box-header with-border">
      <h3 className="box-title">Omset WP <small>per Sudin</small></h3>
    </div>
    <div className="box-body">
      <table id="table_transaction" className="table table-responsive table-bordered table-striped">
        <thead>
          <tr>
            <th className="text-center font9" width="10">#</th><th className="text-center font9">SUDIN</th><th className="text-center font9">OMS</th>
          </tr>
        </thead>
        <tbody>
          {dashboard.omset_sudin.length > 0 ? dashboard.omset_sudin.map((omset, i) => <tr key={i}>
            <td className="font9">{i + 1}</td>
            <td className="font9">{omset.nama_sudin}</td>
            <td className="font9"><span className="pull-right">{Number(omset.omset).toLocaleString("id")}</span></td>
          </tr>) : <tr><td className="text-center font9" colSpan="3" rowSpan="5">BELUM ADA DATA</td></tr>}
        </tbody>
      </table>
    </div>
  </div>
}

export default Dashboard;