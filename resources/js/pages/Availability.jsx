import React, { useState, useEffect} from 'react';
import { useSelector } from 'react-redux';
// import axios from 'axios';
import L from 'leaflet';
import Marquee from 'react-text-marquee';
import PageAlert from '../components/PageAlert';
// import { wilayah } from '../master';
// import store from '../store';
import { getKoordinatWp } from '../services/AvailabilityService';

var map;
var marker_group;

var success_icon = L.icon({
  iconUrl: 'images/success-dot.png',
  iconSize: [8, 8],
  shadowSize: [10, 10]
});

var danger_icon = L.icon({
  iconUrl: 'images/danger-dot.png',
  iconSize: [8, 8],
  shadowSize: [10, 10]
});

const Availability = (props) => {
  const wilayah = useSelector(state => state.auth.wilayah);
  const [alerts, setAlerts] = useState([]);
  const [koordinat_wp, setKoordinatWp] = useState([]);
  const [off_devices, setOffDevices] = useState(undefined);
  const [on_devices, setOnDevices] = useState(undefined);
  const [kode_sudin, setKodeSudin] = useState(wilayah[0].id);
  const [filter, setFilter] = useState(false);
  
  useEffect(() => {
    setKoordinatWp([]);

    const { promise, source } = getKoordinatWp(kode_sudin);
    promise.then(({ koordinat_wp, off_devices, on_devices }) => {
      setKoordinatWp(koordinat_wp);
      setOffDevices(off_devices);
      setOnDevices(on_devices);
      return koordinat_wp;
    })
      .then((koordinat_wp) => {
        var container = L.DomUtil.get('mapid');
        if (container != null) { container._leaflet_id = null; }

        map = L.map('mapid').setView([-6.21462, 106.84513], 12);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        marker_group = L.layerGroup().addTo(map);
        marker_group.clearLayers();
        koordinat_wp.map((v, i) => {
          try {
            L.marker([String(v.latitude).replace(',', '.'), String(v.longitude).replace(',', '.')],
              { icon: v.status == 1 ? success_icon : danger_icon }).addTo(marker_group)
              .bindPopup(renderPopup(v, i));
          }
          catch (err) { }
        });
      })
      .catch(err => { setAlerts([...alerts, { message: err.message }]) });
    return () => {
      map = undefined;
      source.cancel("Request Cancelled");
    }
  }, [filter]);

  function renderPopup(v, i) {
    return `<table>
      <tr><td width="90" valign="top"><label>NOPD</label></td><td valign="top">:&nbsp;</td><td valign="top">${v.nopd}</td></tr>
      <tr><td valign="top"><label>OP</label></td><td valign="top">:&nbsp;</td><td valign="top">${v.nama_wp}</td></tr>
      <tr><td valign="top"><label>Alamat</label></td><td valign="top">:&nbsp;</td><td valign="top">${v.alamat}</td></tr>
      <tr><td valign="top"><label>Availability</label></td><td valign="top">:&nbsp;</td><td valign="top">${v.status == 1 ? "Online" : "Offline"}</td></tr>
      <tr><td valign="top"><label>Reliability</label></td><td valign="top">:&nbsp;</td><td valign="top">${v.status == 1 ? "Online" : "Offline"}</td></tr>
    </table>`
  }

  function onFilterClicked(e) {
    e.preventDefault();
    setFilter(!filter);
  }

  return <div className="content-wrapper">
    <section className="content-header">
      <h1>Availability<small>Perangkat</small></h1>
      <ol className="breadcrumb">
        <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
        <li className="active">Availability</li>
      </ol>
    </section>

    <section className="content" style={{ minHeight: 340 }}>
      <div className="row">
        <div className="col-md-12">
          {
            alerts.map((v, i) =>
              <PageAlert type={v.type} title={v.title} message={v.message} icon={v.icon} key={i} />
            )
          }
        </div>
      </div>

      <div className="row">
        <div className="col-lg-12 col-md-12 col-sm-12">
          <div className="box box-success">
            <div className="box-header">
              <b>ONLINE</b>: <marquee scrollamount="3"><b className="text-success">{on_devices}</b></marquee>
            </div>
          </div>
        </div>

        <div className="col-lg-12 col-md-12 col-sm-12">
          <div className="box box-danger">
            <div className="box-header">
              <b>OFFLINE</b>:<marquee scrollamount="3"><b className="text-danger">{off_devices}</b></marquee>
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-12">
          <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Status Perangkat</h3>
            </div>
            <div className="box-body">
              <div className="col-md-6">
                <form className="form-horizontal">
                  <div className="box-body">
                    <div className="form-group">
                      <label htmlFor="kode_sudin" className="col-md-3 control-label">WILAYAH</label>
                      <div className="col-md-9">
                        <div className="input-group">
                          <select className="form-control" id="kode_sudin" name="kode_sudin"
                            value={kode_sudin}
                            onChange={(e) => setKodeSudin(e.target.value)}>
                            {wilayah.map((v, i) => <option key={i} value={v.id}>{v.wilayah} ({v.kode})</option>)}
                          </select>
                          <span className="input-group-btn">
                            <button type="button" className="btn btn-default btn-flat" onClick={onFilterClicked}>FILTER</button>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div id="mapid" className="col-md-12" style={{ height: "640px" }}></div>
            </div>
            {!(koordinat_wp.length > 0) && <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div>}
          </div>
        </div>
      </div>
    </section>
  </div>
}

export default Availability;