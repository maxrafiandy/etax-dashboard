import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Provider, useDispatch, useSelector } from 'react-redux';
import store from '../store';
import { userDetails } from '../services/UserService';
import { MainPage } from './MainPage';
import { WaitingPage } from './WaitingPage';
import { LoginPage } from './LoginPage';

const App = () => {
  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const token = localStorage.getItem("token");

  useEffect(() => {
    if (token) {
      dispatch({ type: "AUTH_SET_LOADING" });
      userDetails(token).then(resp => {
        const user = resp.user;
        const payloads = { token, username: user.name, level: user.level, menu: user.menu, wilayah: user.wilayah };
        dispatch({ type: 'SIGN_IN', payloads });
        dispatch({ type: "AUTH_UNSET_LOADING" });
      }).catch(e => {
        dispatch({ type: "AUTH_UNSET_LOADING" });
      });
    }
  }, []);

  if (auth.logged_in) return <MainPage />
  return auth.loading ? <WaitingPage /> : <LoginPage />
}

// Main loop
if (document.getElementById('app')) {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('app')
  );
}