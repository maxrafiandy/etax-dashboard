export const wilayah = [
  { id: "01", wilayah: "JAKARTA PUSAT", kode: "P" },
  { id: "03", wilayah: "JAKARTA SELATAN", kode: "S" },
  { id: "05", wilayah: "JAKARTA TIMUR", kode: "T" },
  { id: "07", wilayah: "JAKARTA BARAT", kode: "B" },
  { id: "09", wilayah: "JAKARTA UTARA", kode: "U" }
];

export const suban = [
  { id: 0, wilayah: "BPRD" },
  { id: 1, wilayah: "JAKARTA PUSAT" },
  { id: 3, wilayah: "JAKARTA SELATAN" },
  { id: 5, wilayah: "JAKARTA TIMUR" },
  { id: 7, wilayah: "JAKARTA BARAT" },
  { id: 9, wilayah: "JAKARTA UTARA" }
];

export const bulan = {
  "Jan": "Januari",
  "Feb": "Februari",
  "Mar": "Maret",
  "Apr": "April",
  "May": "Mei",
  "Jun": "Juni",
  "Jul": "Juli",
  "Aug": "Agustus",
  "Sep": "September",
  "Oct": "Oktober",
  "Nov": "November",
  "Dec": "Desember"
};

const etax_service = "http://192.168.43.73:9001/api";
export const agents = {
  // test: `${etax_service}/test/`,
  connected: `${etax_service}/agent/connected`,
  disconnected: `${etax_service}/agent/disconnected`,
  status: {
    index: `${etax_service}/agent/stat`,
    history: `${etax_service}/agent/stat/history`,
    jumlah: `${etax_service}/agent/stat/jumlah`,
  }
};

export default etax_service;