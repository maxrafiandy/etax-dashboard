<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>E-Tax</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('adminlte/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.css') }}">
  <!-- <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/square/blue.css') }}"> -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body class="hold-transition login-page">
  <div id="signin"></div>
  <script src="{{ asset('adminlte/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('adminlte/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js')}}"></script>
  <!-- <script>
    $(function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
      });
    });
  </script> -->
  <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>