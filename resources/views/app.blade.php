<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Monitoring eTax</title>
  @if (isset($_SERVER['HTTPS']))
  <link rel="stylesheet" href="{{ secure_asset('adminlte/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ secure_asset('adminlte/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ secure_asset('adminlte/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ secure_asset('adminlte/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ secure_asset('adminlte/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ secure_asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
  <!-- AdminLTE CDN -->
  @else
  <link rel="shortcut icon" type="image/x-icon" href="etax-dki.png" />
  <link rel="stylesheet" href="{{ asset('adminlte/bootstrap/dist/css/bootstrap.min.css') }}" >
  <link rel="stylesheet" href="{{ asset('adminlte/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
  @endif
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />
  @if (isset($_SERVER['HTTPS']))
  <link rel="stylesheet" href="{{ secure_asset('css/app.css') }}">
  @else
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  @endif
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div id="app"></div>
  @if (isset($_SERVER['HTTPS']))
  <script src="{{ secure_asset('adminlte/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ secure_asset('adminlte/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ secure_asset('adminlte/datatables.net/js/jquery.dataTables.min.js')}} "></script>
  <script src="{{ secure_asset('adminlte/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{ secure_asset('adminlte/dist/js/adminlte.min.js') }}"></script>
  <script src="{{ secure_asset('js/app.js') }}"></script>
  @else
  <script src="{{ asset('adminlte/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('adminlte/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('adminlte/datatables.net/js/jquery.dataTables.min.js')}} "></script>
  <script src="{{ asset('adminlte/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
  <script src="{{ asset('js/app.js') }}"></script>
  @endif
  <script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/natural.js"></script>
</body>

</html>