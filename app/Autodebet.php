<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autodebet extends Model
{
  protected $connection = "monitoring";
  protected $table = "tbl_autodebet";
}
