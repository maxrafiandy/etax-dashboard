<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MirrorPenarikan extends Model
{
  protected $connection = 'monitoring';
  protected $table = 'tbl_mirror_penarikan';
  protected $fillable = [
    'nopd', 'nama_objek_pajak', 'kode_sudin',
    'trx1', 'omset1',
    'trx2', 'omset2',
    'trx3', 'omset3',
    'setoran', 'masapajak', 'tahunpajak'
  ];
}
