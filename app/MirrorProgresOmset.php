<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MirrorProgresOmset extends Model
{
  protected $connection = 'monitoring';
  protected $table = 'tbl_mirror_progres_omset';
  protected $fillable = [
    'nopd', 'nama_objek_pajak', 'kode_sudin',
    'trx1', 'omset1', 'pajak1',
    'trx2', 'omset2', 'pajak2',
    'trx3', 'omset3', 'pajak3',
    'masapajak', 'tahunpajak'
  ];
}
