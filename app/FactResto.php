<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactResto extends Model
{
    protected $connection = "pod";
    protected $table = "tbl_fact_resto";
}
