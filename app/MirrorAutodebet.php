<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MirrorAutodebet extends Model
{
  protected $connection = 'monitoring';
  protected $table = 'tbl_mirror_autodebet';
  protected $fillable = [
    'nopd', 'status', 'autodebet_1',
    'autodebet_2', 'autodebet_3',
    'masapajak', 'tahunpajak'
  ];
}
