<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Npwpd extends Model
{
    protected $connection = "mpod";
    protected $table = "tbl_npwpd";

    public function nopd()
    {
        return $this->hasMany("App\Nopd","npwpd","npwpd");
    }
}
