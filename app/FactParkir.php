<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactParkir extends Model
{
    protected $connection = "pod";
    protected $table = "tbl_fact_parkir";
}
