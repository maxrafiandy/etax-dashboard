<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MpdSspd extends Model
{
    protected $connection = "mpdsspd";
    protected $table = "mpd_sspd";
}