<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryOffline extends Model
{
    // view history_table
    protected $connection = 'monitoring';
    protected $table = 'history_offline';
}
