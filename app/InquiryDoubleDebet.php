<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InquiryDoubleDebet extends Model
{
  protected $connection = 'monitoring';
  protected $table = 'tbl_inquiry_double_debet';
}
