<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SudinPajak extends Model
{
    protected $connection = "mpod";
    protected $table = "tbl_sudin_pajak";

    public function nopd()
    {
        return $this->hasMany("App\Nopd","kode_sudin","kode_sudin");
    }
}
