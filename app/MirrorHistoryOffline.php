<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MirrorHistoryOffline extends Model
{
  protected $connection = 'monitoring';
  protected $table = 'tbl_mirror_history_offline';
  protected $fillable = [
    'nopd', 'npwpd', 'nama_objek_usaha',
    'nama_wajib_pajak', 'alamat', 'kode_sudin',
    'jenis_usaha', 'jenis_penarikan',
    'ipaddresss', 'keterangan', 'status',
    'last_availability', 'last_reliable'
  ];
}
