<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use HasApiTokens, Notifiable;
  protected $fillable = ['email', 'name', 'password', 'level'];
  protected $hidden = ['password', 'remember_token'];
  protected $casts = ['email_verified_at' => 'datetime',];

  public function AauthAcessToken()
  {
    return $this->hasMany('\App\OauthAccessToken');
  }
}
