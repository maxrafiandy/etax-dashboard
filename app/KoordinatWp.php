<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KoordinatWp extends Model
{
    protected $connection = "monitoring";
    protected $table = "koordinat_wp";
    public $timestamps = false;
}