<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KlasifikasiUsaha extends Model
{
    protected $connection = "mpod";
    protected $table = "tbl_klasifikasi_usaha";
}
