<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nopd extends Model
{
    protected $connection = "mpod";
    protected $table = "tbl_nopd";

    public function sudin()
    {
        return $this->belongsTo("App\SudinPajak","kode_sudin", "kode_sudin");
    }

    public function npwpd()
    {
        return $this->belongsTo("App\Npwpd", "npwpd", "npwpd");
    }
}
