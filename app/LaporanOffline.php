<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanOffline extends Model
{
    protected $connection = 'monitoring';
    protected $table = 'tbl_laporan_offline';
    protected $fillable = [
        'nopd', 'attachment', 'keterangan'
    ];
}
