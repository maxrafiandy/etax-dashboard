<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class DbViewController extends Controller
{
  private $qview_koordinat_wp = "SELECT 
      n.nopd,
      n.npwpd,
      n.nama_objek_usaha,
      n.alamat,
      coalesce(g.latitude, 0.0) latitude,
      coalesce(g.longitude, 0.0) longitude,
      n.kode_sudin,
      n.status_live,
      coalesce(g.iphost, 'N/A') ipaddress,
      0 status,
      GETDATE() last_status
      from MASTER_PAJAK_ONLINE_DKI.dbo.TBL_NOPD n
      left join (
        select NOPD, IPHOST, latitude, longitude
        from PAJAKGRABBER_RESTO.dbo.masterinstitusi union 
        select NOPD, IPHOST, latitude, longitude
        from PAJAKGRABBER_PARKIR.dbo.masterinstitusi union 
        select NOPD, IPHOST, latitude, longitude
        from PAJAKGRABBER_HIBURAN.dbo.masterinstitusi
      ) g on g.nopd = n.nopd
      WHERE UPPER(n.nama_objek_usaha) NOT LIKE '%TUTUP%'
      AND n.status_live in (4,5,6)";

  public function viewKoordinatWp($limit = 500)
  {
    $koordinat_wp = DB::select($this->qview_koordinat_wp . ' limit ? ?', [0, $limit]);
    return response()->json($koordinat_wp, 200);
  }
}