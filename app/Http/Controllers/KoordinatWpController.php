<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use App\KoordinatWp;
use DB;

class KoordinatWpController extends Controller
{
  public function koordinatWp($kode_sudin = "01")
  {
    $response = new \stdClass();

    $response->koordinat_wp = KoordinatWp::select(
      "nopd",
      "npwpd",
      "nama_objek_usaha as nama_wp",
      "alamat",
      "longitude",
      "latitude",
      "ipaddress",
      "kode_sudin",
      "status"
    )->where("kode_sudin", $kode_sudin)->get();
    return response()->json($response, 200);
  }

  public function offlineOnlineSudin()
  {
    $response = new \stdClass();

    $response->status_sudin = DB::connection("monitoring")
      ->table("koordinat_wp as kwp")
      ->leftJoin("master_pajak_online_dki.dbo.tbl_sudin_pajak as sudins")
      ->select(
        "kwp.kode_sudin",
        "sudin.nama_sudin",
        DB::raw("(SELECT COUNT(kode_sudin) FROM koordinat_wp wp2 WHERE wp2.status=0 AND wp2.kode_sudin = koordinat_wp.kode_sudin) AS jumlah_offline"),
        DB::raw("(SELECT COUNT(kode_sudin) FROM koordinat_wp wp3 WHERE wp3.status=1 AND wp3.kode_sudin = koordinat_wp.kode_sudin) AS jumlah_online")
      )->groupBy("kode_sudin", "nama_sudin")
      ->get();
    return response()->json($response, 200);
  }
}
