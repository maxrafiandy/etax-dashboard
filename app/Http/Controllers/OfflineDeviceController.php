<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MirrorHistoryOffline;
use App\LaporanOffline;
// use App\HistoryOffline;
use Illuminate\Support\Facades\Storage;
use Validator;

class OfflineDeviceController extends Controller
{
  public function offlineDevices($kode_sudin = "01")
  {
    $response = new \stdClass;

    $response->offline_devices = MirrorHistoryOffline::select(
      "tbl_mirror_history_offline.*",
      "lof.attachment",
      "lof.keterangan"
    )
      ->leftJoin('tbl_laporan_offline as lof', 'lof.nopd', '=', 'tbl_mirror_history_offline.nopd')
      ->where("status", "OFFLINE")
      ->where("kode_sudin", $kode_sudin)
      ->orderBy("last_availability")
      ->get();

    return response()->json($response, 200);
  }

  public function uploadBap(Request $request)
  {
    $validator = Validator::make(
      $request->all(),
      ['nopd' => 'required', 'keterangan' => 'required', 'file' => 'required|file|mimes:pdf']
    );

    if ($validator->fails()) {
      return response()->json(['error' => $validator->errors()], 406);
    }

    if ($request->hasFile('file')) {
      $images = time() . '_' . str_replace(".", "", $request->nopd) . ".pdf";
      $path = $request->file->storeAs('public/images/baps', $images);
      $laporan_offline = LaporanOffline::create(
        ['nopd' => $request->nopd, 'attachment' => $path, 'keterangan' => $request->keterangan]
      );
      return response()->json($laporan_offline, 200);
    }
    return response()->json(['error' => "No update"], 500);
  }

  public function getBap($id)
  {
    try {
      $laporan_offline = LaporanOffline::find($id);
      if (isset($laporan_offline)) {
        if ($laporan_offline->attachment) {
          $file = Storage::download($laporan_offline->attachment);
          if (isset($file)) {
            return $file;
          }
        }
      }
    } catch (\Exception $ex) {
      return response("Page not found", 404);
    }
  }

  public function getOfflineHistory($nopd)
  {
    try {
      $laporan_offline = LaporanOffline::where("nopd", $nopd)->get();
      $response = new \stdClass;
      $response->history = $laporan_offline;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      return response()->json(["error" => $ex], 500);
    }
  }
}
