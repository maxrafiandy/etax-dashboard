<?php

namespace App\Http\Controllers;

use App\MpdSspd;
use Illuminate\Http\Request;
use DB;

class MpdSspdController extends Controller
{
    // Data 3 bulan terakhir
    public function index() 
    {
        $response = new \stdClass();
        $response->mpdsspd = MpdSspd::select(
            "NOP as nop", 
            "MASAPAJAK as masa_pajak", 
            "TAHUNPAJAK as tahun_pajak", 
            "TRANSDATETIME as trans_datetime"
        )->limit(5000)
        ->orderBy("TAHUNPAJAK","DESC")
        ->orderBy("MASAPAJAK","DESC")->get();
        
        return response()->json($response, 200); 
    }

    public function show($id)
    {
        $response = new \stdClass();
        $response->mpdsspd = MpdSspd::select(
            "NOP as nop", 
            "MASAPAJAK as masa_pajak", 
            "TAHUNPAJAK as tahun_pajak", 
            "TRANSDATETIME as trans_datetime"
        )->find($id);
        
        return response()->json($response, 200); 
    }

    public function store(Request $request) 
    {
        return response()->json($request, 200);
    }

    public function update(Request $request, $id)
    {
        return response()->json($request, 200);
    }
}
