<?php

namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SudinController extends Controller
{
  public function omset($kode_sudin = 0)
  {
    $qb = DB::connection("monitoring")->table("tbl_mirror_progres_omset as pgomset")->join("master_pajak_online_dki.dbo.tbl_sudin_pajak as sp", "pgomset.kode_sudin", "sp.kode_sudin");
    if ($kode_sudin == 0) {
      $omset = $qb->select("pgomset.kode_sudin", "sp.nama_sudin", DB::raw("SUM(pgomset.omset3) omset"))->groupBy("pgomset.kode_sudin", "sp.nama_sudin")->get();
      return response()->json(["omsets" => $omset], 200);
    }

    $omset = $qb->select("pgomset.kode_sudin", "sp.kode_sudin", DB::raw("SUM(pgomset.omset3) omset"))->where("sp.kode_sudin", $kode_sudin)->groupBy("pgomset.kode_sudin", "sp.nama_sudin")->first();
    return response()->json($omset, 200);
  }

  public function totalOp($kode_sudin = 0)
  {
    $qb = DB::connection("monitoring")->table("tbl_mirror_progres_omset as pgomset");
    if ($kode_sudin == 0) {
      $total_ops = $qb->select("pgomset.kode_sudin", DB::raw("COUNT(1) total_op"))->groupBy("pgomset.kode_sudin")->get();
      return response()->json(["total_ops" => $total_ops], 200);
    }

    $total_op = $qb->select("kode_sudin", DB::raw("COUNT(1) total_op"))->where("kode_sudin", $kode_sudin)->groupBy("kode_sudin")->first();
    return response()->json($total_op, 200);
  }
}
