<?php

namespace App\Http\Controllers\v2;

// use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class JenisUsahaController extends Controller
{
  public function omset($kode_jenis_usaha = '')
  {
    $qb = DB::connection("monitoring")->table("tbl_mirror_progres_omset as pgomset");
    if ($kode_jenis_usaha == '') {
      $omset = $qb->select("kode_jenis_usaha", DB::raw("SUM(omset3) omset"))->
        join("master_pajak_online_dki.dbo.tbl_nopd", "tbl_nopd.nopd", "pgomset.nopd")->
        groupBy("kode_jenis_usaha")->get();
      return response()->json(["omsets" => $omset], 200);
    }

    $omset = $qb->select("kode_jenis_usaha", DB::raw("SUM(omset3) omset"))->
    join("master_pajak_online_dki.dbo.tbl_nopd", "tbl_nopd.nopd", "pgomset.nopd")->
      where("kode_jenis_usaha", $kode_jenis_usaha)->groupBy("kode_jenis_usaha")->first();
    return response()->json($omset, 200);
  }

  public function totalOp($kode_jenis_usaha = '') {
    $qb = DB::connection("monitoring")->table("tbl_mirror_progres_omset as pgomset");
    if ($kode_jenis_usaha == '') {
      $total_op = $qb->select("kode_jenis_usaha", DB::raw("COUNT(1) total_op"))->
        join("master_pajak_online_dki.dbo.tbl_nopd", "tbl_nopd.nopd", "pgomset.nopd")->
        where("omset3", ">", 0)->
        groupBy("kode_jenis_usaha")->get();
      return response()->json(["total_op" => $total_op], 200);
    }

    $total_op = $qb->select("kode_jenis_usaha", DB::raw("COUNT(1) total_op"))->
    join("master_pajak_online_dki.dbo.tbl_nopd", "tbl_nopd.nopd", "pgomset.nopd")->
      where("kode_jenis_usaha", $kode_jenis_usaha)->where("omset3", ">", 0)->
      groupBy("kode_jenis_usaha")->first();
    return response()->json($total_op, 200);
  }
}
