<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\MirrorAutodebet;
use App\MirrorProgresOmset;
use App\MirrorPenarikan;
use App\MirrorHistoryOffline;
use PHPUnit\Runner\Exception;

class MirroringController extends Controller
{
  private function getAutodebetQuery($props)
  {
    return "SELECT *, '{$props->months[2]}' masapajak, '{$props->years[2]}' tahunpajak 
    FROM (
      (
        SELECT master.*, 
        3 status,
        COALESCE(sukses1.transactionamount, 0) autodebet_1, 
        COALESCE(sukses2.transactionamount, 0) autodebet_2, 
        COALESCE(sukses3.transactionamount, 0) autodebet_3
        FROM
        (
          SELECT nop
          FROM mpd_sspd
          WHERE nop is not null
          GROUP BY nop
        ) master
        -- sukses 3 bulan terakhir
        left join (
          SELECT nop, status, sum(transactionamount) as transactionamount 
          FROM mpd_sspd
          WHERE nop is not null
          AND STATUS = 3
          AND masapajak = {$props->months[0]}
          AND tahunpajak = {$props->years[0]}
          GROUP BY nop, status
        ) sukses1 on sukses1.nop = master.nop
        -- sukses 2 bulan terakhir
        left join (
          SELECT nop, status, sum(transactionamount) as transactionamount 
          FROM mpd_sspd
          WHERE nop is not null
          AND STATUS = 3
          AND masapajak = {$props->months[1]}
          AND tahunpajak = {$props->years[1]}
          GROUP BY nop
        ) sukses2 on sukses2.nop = master.nop
        -- sukses 1 bulan terakhir
        left join (
          SELECT nop, status, sum(transactionamount) as transactionamount 
          FROM mpd_sspd
          WHERE nop is not null
          AND STATUS = 3
          AND masapajak = {$props->months[2]}
          AND tahunpajak = {$props->years[2]}
          GROUP BY nop
        ) sukses3 on sukses3.nop = master.nop
      )
      UNION
      (
        SELECT master.*, 
        4 status,
        COALESCE(sukses1.transactionamount, 0) autodebet_1, 
        COALESCE(sukses2.transactionamount, 0) autodebet_2, 
        COALESCE(sukses3.transactionamount, 0) autodebet_3
        FROM
        (
          SELECT nop
          FROM mpd_sspd
          WHERE nop is not null
          GROUP BY nop
        ) master
        -- sukses 3 bulan terakhir
        left join (
          SELECT nop, 4 status, sum(transactionamount) as transactionamount 
          FROM mpd_sspd
          WHERE nop is not null
          AND STATUS != 3
          AND masapajak = {$props->months[0]}
          AND tahunpajak = {$props->years[0]}
          GROUP BY nop, status
        ) sukses1 on sukses1.nop = master.nop
        -- sukses 2 bulan terakhir
        left join (
          SELECT nop, 4 status, sum(transactionamount) as transactionamount 
          FROM mpd_sspd
          WHERE nop is not null
          AND STATUS != 3
          AND masapajak = {$props->months[1]}
          AND tahunpajak = {$props->years[1]}
          GROUP BY nop
        ) sukses2 on sukses2.nop = master.nop
        -- sukses 1 bulan terakhir
        left join (
          SELECT nop, 4 status, sum(transactionamount) as transactionamount 
          FROM mpd_sspd
          WHERE nop is not null
          AND STATUS != 3
          AND masapajak = {$props->months[2]}
          AND tahunpajak = {$props->years[2]}
          GROUP BY nop
        ) sukses3 on sukses3.nop = master.nop
      )
    ) mpd_sspd
    WHERE autodebet_1 != 0
    AND autodebet_2 != 0
    AND autodebet_3 != 0";
  }

  private function getHistoryOfflineQuery()
  {
    return
      "SELECT
        nop.nopd,
        nop.npwpd,
        nop.nama_objek_usaha,
        npd.nama_wajib_pajak,
        nop.alamat,
        nop.kode_sudin,
        case nop.kode_jenis_usaha
          when 'R' then 'RESTO'
          when 'H' then 'HIBURAN'
          when 'T' then 'HOTEL'
          when 'P' then 'PARKIR'
        end as jenis_usaha,
        case nop.status_live
          when '4' then 'TERPASANG'
          when '5' then 'TERKONEKSI'
          when '6' then 'AUTODEBET'
        end as jenis_penarikan,
        kwp.ipaddress,
        kwp.keterangan,
        case kwp.status
          when 1 then 'ONLINE'
          else 'OFFLINE' 
        end as status,
        case kwp.status
          when 1 then getdate()
          else null
        end as last_availability,
        coalesce(
          rby.tanggal_trx, 
          null) as last_reliable
      from master_pajak_online_dki.dbo.tbl_nopd as nop
      left join monitoring.dbo.koordinat_wp as kwp on kwp.nopd = nop.nopd
      
      -- left join (
      --	select nopd, max(tanggal_trx) tanggal_trx from pajak_online_dki_201906.dbo.TBL_FACT_RESTO group by nopd
      --	union
      --	select nopd, max(tanggal_trx) tanggal_trx from pajak_online_dki_201906.dbo.TBL_FACT_HIBURAN group by nopd
      --	union
      --	select nopd, max(tanggal_trx) tanggal_trx from pajak_online_dki_201906.dbo.TBL_FACT_HOTEL group by nopd
      --	union
      --	select nopd, max(waktu_keluar) tanggal_trx from pajak_online_dki_201906.dbo.TBL_FACT_PARKIR group by nopd
      -- ) rby on rby.nopd = nop.nopd

      left join (select nopd, max(LASTUPDATE_TODAY) tanggal_trx from PAJAK_ONLINE_DKI_201906.dbo.TBL_SUM_NOPD group by nopd) 
      rby on rby.nopd = nop.nopd 
      
      left join master_pajak_online_dki.dbo.tbl_npwpd as npd on npd.npwpd = nop.npwpd
      where nop.status_live in (4,5,6)
      and UPPER(nop.nama_objek_usaha) NOT LIKE '%TUTUP%'
      and kwp.status != 1
      group by nop.nopd, nop.npwpd, nop.nama_objek_usaha, npd.nama_wajib_pajak,
        nop.alamat, nop.kode_sudin, nop.kode_jenis_usaha,
        nop.status_live, kwp.ipaddress, 
        kwp.keterangan, kwp.status, rby.tanggal_trx, kwp.last_status";
  }

  private function mirrorAutodebet()
  {
    for ($i = 3; $i >= 1; $i--) {
      $months[] = date("m", strtotime(date('Y-m') . " -$i months"));
      $years[] = date("Y", strtotime(date('Y-m') . " -$i months"));
    }

    try {
      $props = new \stdClass;
      $props->months = $months;
      $props->years = $years;
      $query = $this->getAutodebetQuery($props);
      $mpdsspd = DB::connection("brichannel")->select($query);

      MirrorAutodebet::truncate();
      foreach ($mpdsspd as $m) {
        MirrorAutodebet::create([
          'nopd' => $m->nop,
          'status' => $m->status,
          'masapajak' => $m->masapajak,
          'tahunpajak' => $m->tahunpajak,
          'autodebet_1' => $m->autodebet_1,
          'autodebet_2' => $m->autodebet_2,
          'autodebet_3' => $m->autodebet_3
        ]);
      }
      return "SUKSES";
    } catch (Exception $e) {
      return $e->getMessage();
    }
  }

  private function mirrorProgresOmset()
  {
    for ($i = 2; $i >= 1; $i--) {
      $months[] = date("m", strtotime(date('Y-m') . " -$i months"));
      $years[] = date("Y", strtotime(date('Y-m') . " -$i months"));
    }

    try {
      $progres_omset = DB::connection("mpod")->table("master_pajak_online_dki.dbo.tbl_nopd as nop")
        ->select(
          "nop.nopd",
          "nop.nama_objek_usaha as nama_objek_pajak",
          "nop.kode_sudin",
          DB::raw("coalesce(sum1.sum_trx,0) as trx1"),
          DB::raw("coalesce(sum1.sum_omset,0) as omset1"),
          DB::raw("coalesce(sum1.sum_pajak,0) as pajak1"),
          DB::raw("coalesce(sum2.sum_trx,0) as trx2"),
          DB::raw("coalesce(sum2.sum_omset,0) as omset2"),
          DB::raw("coalesce(sum2.sum_pajak,0) as pajak2"),
          DB::raw("coalesce(sum3.sum_trx,0) as trx3"),
          DB::raw("coalesce(sum3.sum_omset,0) as omset3"),
          DB::raw("coalesce(sum3.sum_pajak,0) as pajak3")
        )
        ->leftJoin(
          "pajak_online_dki_" . $years[0] . $months[0] . ".dbo.tbl_sum_nopd as sum1",
          "sum1.nopd",
          "nop.nopd"
        )
        ->leftJoin(
          "pajak_online_dki_" . $years[1] . $months[1] . ".dbo.tbl_sum_nopd as sum2",
          "sum2.nopd",
          "nop.nopd"
        )
        ->leftJoin(
          "pajak_online_dki" . ".dbo.tbl_sum_nopd as sum3",
          "sum3.nopd",
          "nop.nopd"
        )
        ->get();

      MirrorProgresOmset::truncate();
      foreach ($progres_omset as $po) {
        MirrorProgresOmset::create([
          'nopd' => $po->nopd,
          'nama_objek_pajak' => $po->nama_objek_pajak,
          'kode_sudin' => $po->kode_sudin,
          'trx1' => $po->trx1,
          'omset1' => $po->omset1,
          'pajak1' => $po->pajak1,
          'trx2' => $po->trx2,
          'omset2' => $po->omset2,
          'pajak2' => $po->pajak2,
          'trx3' => $po->trx3,
          'omset3' => $po->omset3,
          'pajak3' => $po->pajak3,
          'masapajak' => $months[1],
          'tahunpajak' => $years[1]
        ]);
      }
      return "SUKSES";
    } catch (Exception $e) {
      return $e->getMessage();
    }
  }

  private function mirrorPenarikan()
  {
    $month = date("m", strtotime(date('Y-M') . " -1 months"));
    $year  = date("Y", strtotime(date('Y-M') . " -1 months"));

    try {
      $penarikan = DB::connection("mpod")
        ->table("master_pajak_online_dki.dbo.tbl_nopd as nop")
        ->leftJoin("pajak_online_dki_" . $year . $month . ".dbo.tbl_sum_nopd as sum", "sum.nopd", "nop.nopd")
        ->leftJoin("pajak_online_dki_" . $year . $month . ".dbo.tbl_adjust_manual as adj", "adj.nopd", "nop.nopd")
        ->leftJoin("pajak_online_dki_" . $year . $month . ".dbo.tbl_sum_nopd as sum_pod", "sum_pod.nopd", "nop.nopd")
        ->leftJoin("pajak_online_dki.dbo.tbl_fact_pembayaran_riil as riil", "riil.nopd", "nop.nopd")
        ->select(
          "nop.nopd",
          "nop.nama_objek_usaha as nama_objek_pajak",
          "nop.kode_sudin",
          DB::raw("coalesce(sum.sum_trx,0) as trx1"),
          DB::raw("coalesce(sum.sum_omset,0) as omset1"),
          DB::raw("coalesce(count(adj.nopd),0) as trx2"),
          DB::raw("coalesce(sum(adj.amount_adjust),0) as omset2"),
          DB::raw("coalesce(sum_pod.sum_trx,0) as trx3"),
          DB::raw("coalesce(sum_pod.sum_omset,0) as omset3"),
          DB::raw("coalesce(sum(riil.setoran_pajak),0) as setoran"),
          DB::raw("'$month' as masapajak"),
          DB::raw("'$year' as tahunpajak")
        )
        ->where("nop.nama_objek_usaha", "not like", "%TUTUP%")
        ->groupBy("nop.nopd", "nop.nama_objek_usaha", "nop.kode_sudin", "sum.sum_trx", "sum.sum_omset", "sum_pod.sum_trx", "sum_pod.sum_omset")
        ->get();

      MirrorPenarikan::truncate();
      foreach ($penarikan as $pn) {
        MirrorPenarikan::create([
          'nopd' => $pn->nopd,
          'nama_objek_pajak' => $pn->nama_objek_pajak,
          'kode_sudin' => $pn->kode_sudin,
          'trx1' => $pn->trx1,
          'omset1' => $pn->omset1,
          'trx2' => $pn->trx2,
          'omset2' => $pn->omset2,
          'trx3' => $pn->trx3,
          'omset3' => $pn->omset3,
          'setoran' => $pn->setoran,
          'masapajak' => $pn->masapajak,
          'tahunpajak' => $pn->tahunpajak,
        ]);
      }
      return "SUKSES";
    } catch (Exception $e) {
      return $e->getMessage();
    }
  }

  private function mirrorHistoryOffline()
  {
    try {
      $query = $this->getHistoryOfflineQuery();
      $history = DB::connection("monitoring")->select($query);
      MirrorHistoryOffline::truncate();
      foreach ($history as $h) {
        MirrorHistoryOffline::create([
          'nopd' => $h->nopd,
          'npwpd' => $h->npwpd,
          'nama_objek_usaha' => $h->nama_objek_usaha,
          'nama_wajib_pajak' => $h->nama_wajib_pajak,
          'alamat' => $h->alamat,
          'kode_sudin' => $h->kode_sudin,
          'jenis_usaha' => $h->jenis_usaha,
          'jenis_penarikan' => $h->jenis_penarikan,
          'ipaddress' => $h->ipaddress,
          'keterangan' => $h->keterangan,
          'status' => $h->status,
          'last_availability' => $h->last_availability,
          'last_reliable' => $h->last_reliable,
        ]);
      }
      return "SUKSES";
    } catch (Exception $e) {
      return $e->getMessage();
    }
  }

  public function cronDaily()
  {
    $autodebet = $this->mirrorAutodebet();
    $progres_omset = $this->mirrorProgresOmset();
    $penarikan = $this->mirrorPenarikan();
    $history_offline = $this->mirrorHistoryOffline();

    return response()->json([
      "autodebet" => $autodebet,
      "progres_omset" => $progres_omset,
      "penarikan" => $penarikan,
      "history_offline" => $history_offline
    ], 200);
  }
}
