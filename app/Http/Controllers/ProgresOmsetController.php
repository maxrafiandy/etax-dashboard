<?php

namespace App\Http\Controllers;

use App\MirrorProgresOmset;
use App\Nopd;
use App\FactHotel;
use App\FactHiburan;
use App\FactParkir;
use App\FactResto;

class ProgresOmsetController extends Controller
{
  public function progresOmsetSudin($kode_sudin)
  {
    $tmp = 2;
    for ($i = $tmp; $i > 0; $i--) {
      $periods[$tmp - $i] = new \stdClass;
      $periods[$tmp - $i]->long_month = date("M", strtotime(date('Y-M') . " -$i months"));
      $periods[$tmp - $i]->short_month = date("m", strtotime(date('Y-M') . " -$i months"));
      $periods[$tmp - $i]->year = date("Y", strtotime(date('Y-M') . " -$i months"));
    }
    $progres_omset = MirrorProgresOmset::where("kode_sudin", $kode_sudin)
      ->where(function ($query) {
        $query->where("trx1", ">", 0)->orWhere("trx2", ">", 0)->orWhere("trx3", ">", 0);
      })
      ->orderBy("omset3", "desc")->get();
    return response()->json([
      "progres_omset" => $progres_omset,
      "periods" => $periods
    ], 200);
  }

  public function detailOmset($nopd)
  {
    $op = Nopd::where("nopd", $nopd)->first();
    if (!$op) return response()->json(["message" => "No query results for model"], 404);
    $jenis_usaha = $op->KODE_JENIS_USAHA;

    switch ($jenis_usaha) {
      case "T":
        $facts = FactHotel::select(
          "TANGGAL_TRX",
          "RECEIPT_NO",
          "DESCRIPTION",
          "AMOUNT_TRX"
        )->where("NOPD", $nopd)->orderBy("TANGGAL_TRX", "desc")->get();
        break;
      case "H":
        $facts = FactHiburan::select(
          "TANGGAL_TRX",
          "RECEIPTNO",
          "AMOUNT_KAMAR",
          "AMOUNT_MAKANMINUM",
          "AMOUNT_COVERCHARGE",
          "AMOUNT_KARCIS",
          "AMOUNT_SCORECOIN",
          "AMOUNT_LAINNYA"
        )->where("NOPD", $nopd)->orderBy("TANGGAL_TRX", "desc")->get();
        break;
      case "R":
        $facts = FactResto::select(
          "TANGGAL_TRX",
          "RECEIPTNO",
          "AMOUNT_TRX",
          "AMOUNT_SERVICE"
        )->where("NOPD", $nopd)->orderBy("TANGGAL_TRX", "desc")->get();
        break;
      case "P":
        $facts = FactParkir::select(
          "JENIS_KENDARAAN",
          "NOPOL",
          "WAKTU_MASUK",
          "WAKTU_KELUAR",
          "AMOUNT_TRX"
        )->where("NOPD", $nopd)->orderBy("WAKTU_KELUAR", "desc")->get();
        break;
    }

    return response()->json(["jenis_usaha" => $jenis_usaha, "detail" => $facts]);
  }
}
