<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nopd;
use DB;

class NopdController extends Controller
{
    // data 3 bulan terakhir
    public function index() 
    {
        $response = new \stdClass(); 
        $response->nopd = DB::connection("mpod")
            ->table("tbl_nopd as no")
            ->select("nopd", "npwpd", "tarif_pajak_201506 as tarif_pajak", "nama_sudin")
            ->join("tbl_klasifikasi_usaha as ku", function($join) {
                $join->on("no.kode_klasifikasi","ku.kode_klasifikasi");
                $join->on("no.kode_jenis_usaha","ku.kode_jenis_usaha");
            })
            ->join("tbl_sudin_pajak as sp", "sp.kode_sudin","=","no.kode_sudin")
            ->limit(100)->get();

        return response()->json($response, 200);
    }

    public function show($id)
    {
        return response()->json($id, 200);
    }

    public function store(Request $request) 
    {
        return response()->json($request, 200);
    }

    public function update(Request $request, $id)
    {
        return response()->json($request, 200);
    }
}