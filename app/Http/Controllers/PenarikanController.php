<?php

namespace App\Http\Controllers;

use App\MirrorPenarikan;

class PenarikanController extends Controller
{
    public function __invoke($kode_sudin = "01")
    {
        $response = new \stdClass;
        /***********************************************
        $month = date("m", strtotime(date('Y-M') . " -1 months"));
        $year  = date("Y", strtotime(date('Y-M') . " -1 months"));

        $response->year = $year;
        $response->month = $month;

        $response->penarikan = DB::connection("mpod")
        ->table("master_pajak_online_dki.dbo.tbl_nopd as nop")
        ->leftJoin("pajak_online_dki_" . $year . $month . ".dbo.tbl_sum_nopd as sum", "sum.nopd", "nop.nopd")
        ->leftJoin("pajak_online_dki_" . $year . $month . ".dbo.tbl_adjust_manual as adj", "adj.nopd", "nop.nopd")
        ->leftJoin("pajak_online_dki_" . $year . $month . ".dbo.tbl_sum_nopd as sum_pod", "sum_pod.nopd", "nop.nopd")
        ->leftJoin("pajak_online_dki.dbo.tbl_fact_pembayaran_riil as riil", "riil.nopd", "nop.nopd")
        ->select(
            "nop.nopd",
            "nop.nama_objek_usaha as nama_objek_pajak",
            DB::raw("coalesce(sum.sum_trx,0) as trx1"),
            DB::raw("coalesce(sum.sum_omset,0) as omset1"),
            DB::raw("coalesce(count(adj.nopd),0) as trx2"),
            DB::raw("coalesce(sum(adj.amount_adjust),0) as omset2"),
            DB::raw("coalesce(sum_pod.sum_trx,0) as trx3"),
            DB::raw("coalesce(sum_pod.sum_omset,0) as omset3"),
            DB::raw("coalesce(sum(riil.setoran_pajak),0) as setoran")
        )
        ->where("nop.kode_sudin", $kode_sudin)
        ->where("nop.nama_objek_usaha", "not like", "%TUTUP%")
        ->groupBy("nop.nopd", "nop.nama_objek_usaha", "sum.sum_trx", "sum.sum_omset", "sum_pod.sum_trx", "sum_pod.sum_omset")
        ->get();
         ***********************************************/

        $response->month = date("M", strtotime(date('Y-M') . " -1 months"));
        $response->year  = date("Y", strtotime(date('Y-M') . " -1 months"));
        $response->penarikan = MirrorPenarikan::where('kode_sudin', $kode_sudin)
            // ->where("trx1", "!=", "0")->where("trx2", "!=", "0")->where("trx3", "!=", "0")
            ->get();

        return response()->json($response, 200);
    }
}
