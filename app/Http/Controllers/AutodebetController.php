<?php

namespace App\Http\Controllers;

use DB;

class AutodebetController extends Controller
{
  public function __invoke($kode_sudin = "01", $status = 3)
  {
    $nopds = DB::connection("mpod")
      ->table("tbl_nopd as no")
      ->select(
        "no.nopd",
        "no.nama_objek_usaha",
        "mad.autodebet_1",
        "mad.autodebet_2",
        "mad.autodebet_3",
        "mad.masapajak",
        "mad.tahunpajak"
      )
      ->leftJoin(\config("database.monitoring").".dbo.tbl_mirror_autodebet as mad", "mad.nopd", DB::raw("replace(no.nopd,'.','')"))
      ->where("no.kode_sudin", $kode_sudin)
      ->where("mad.status", $status)
      ->get();

    for ($i = 1; $i <= 3; $i++) {
      $months[] = date("M", strtotime(date('Y-M') . " -$i months"));
      $years[] = date("Y", strtotime(date('Y-m') . " -$i months"));
    }

    return response()->json(['months' => $months, 'years' => $years, 'nopds' => $nopds], 200);
  }
}
