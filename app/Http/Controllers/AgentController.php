<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AgentController extends Controller
{
  const BASE_URL = 'http://192.168.43.73:9000/api';
  const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjUyNmEyYjU4YzcyYWRlYWQ1NDI3NTBlOTA5NWIyNmI3YTNmMGRkYWY1ZjgyYzQ3M2IxYzc4MGI1ZTQ4MDIzNGI1NTI3ZmNlY2RjZDdiMTk1In0.eyJhdWQiOiI5IiwianRpIjoiNTI2YTJiNThjNzJhZGVhZDU0Mjc1MGU5MDk1YjI2YjdhM2YwZGRhZjVmODJjNDczYjFjNzgwYjVlNDgwMjM0YjU1MjdmY2VjZGNkN2IxOTUiLCJpYXQiOjE1NjU1ODM1MTMsIm5iZiI6MTU2NTU4MzUxMywiZXhwIjoxNTk3MjA1OTEzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.dqSCpxvxXTkrgYFxozpcB2JHz5bo2rDw73Cjg2-e0mVOdmf2z0yOnSemJv_7nsZgNfTt6ZF5H33whU2GXyfgB6u2pQF5Aj2K_8pJlhy-yNegsifgwAAqfZfZ_PjpxI3mBNvO_qbbAgn4f1VJH42wyRCyefxuMLY_64gF6I5lTloLtof4ToceMmkIWo2vtoCE8_Y5p_Y_aiYoJkEmvzB6sXxQHm89hIO7H3Tr9q-XKEv7ZkbHAtCi0fIfhLO2hY9k2DacQrYoexLw4axIoBgI5TnBHnCs3cOElIt_Ef4UVbvZStcs5ezfv6IQ1X_UHy9wiwtuwbcalk91gng2CVjWB7q6yWsDL-wnKaJEfvQLzOfdWwdunJAxSum8o4iYdRv4RHe4XwboSG9FSb1m2Sc8DwnrhChbHCJ9ltYbzxbWvC-L8TD6QplON7JPjCnFu11h6H7sdEXHDSKQH0GSC-m0aa4F3nSE60G-buKFaS83PY32XJMx0prbRKA-avJd8DsBPAEwdsIzLmRkzwktJO_HgQ9Wab71nakubRUlKCf06W5LxYnmLkwnVFSxn3dgxaOFCuwuSpldoK0IrDlRJgVf57rvhibJcjVTzq_xsnEOu9pNMWfquKr5b_WWv-sZ1eLnltQt6GaBt8kKe_mZSF0YdpZl33bi-1oWAE620Shpivo";
  const URLS = [
    'status' => self::BASE_URL.'/agent/status',
  ];

  public function agentStatus()
  {
    try {
      $client = new \GuzzleHttp\Client;
      $response = $client->get(self::URLS['status'], [
        'headers' => [
          'Accept' => 'application/json',
          'Authorization' => self::TOKEN
        ]
      ]);
      echo $response->getBody();
    } catch (\Exception $e) {
      return response()->json(["message" => $e->getMessage()], 500);
    }
  }
}
