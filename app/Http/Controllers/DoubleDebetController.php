<?php

namespace App\Http\Controllers;

use DB;
// use App\InquiryDoubleDebet;

class DoubleDebetController extends Controller
{
  public function doubleDebet()
  {
    $today = date("Y-m-d");
    $first = date("Y-m-01");

    $query = "SELECT 
      nop.nopd, nop.nama_objek_usaha, 
      mutasi_kredit pajak, 
      total_kredit total_debet, 
      COALESCE(total_debet,0) total_reversal, 
      jml_kredit jml_debet, COALESCE(jml_debet, 0) jml_reversal,
      CASE 
        WHEN mutasi_kredit = total_kredit - total_debet THEN 'TIDAK DOUBLE DEBET'
        WHEN total_kredit - total_debet = 0 THEN 'BELUM BAYAR'
        WHEN mutasi_kredit > total_kredit - total_debet THEN 'DEFISIT'
        -- WHEN mutasi_kredit < total_kredit - total_debet THEN 'DOUBLE DEBET'
        ELSE 'DOUBLE DEBET'
      END keterangan
    FROM
    (
      SELECT SUBSTRING(desk_tran, 24, 13) nopd, mutasi_kredit, COUNT(1) jml_kredit, sum(mutasi_kredit) total_kredit
      FROM tbl_inquiry_debet
      WHERE desk_tran like 'AD PAJAK DKI%'
      AND tgl_tran BETWEEN '$first' AND '$today'
      AND mutasi_debet = 0
      GROUP BY SUBSTRING(desk_tran, 24, 13), mutasi_kredit
      HAVING COUNT(1) > 1
    ) kredit
    LEFT JOIN 
    (
      SELECT SUBSTRING(desk_tran, 24, 13) nopd, mutasi_debet, COUNT(1) jml_debet, sum(mutasi_debet) total_debet
      FROM tbl_inquiry_debet
      WHERE desk_tran like 'AD PAJAK DKI%'
      AND tgl_tran BETWEEN '$first' AND '$today'
    -- 	AND kode_tran = '88'
      GROUP BY SUBSTRING(desk_tran, 24, 13), mutasi_debet
    ) debet ON kredit.nopd = debet.nopd AND mutasi_debet = mutasi_kredit
    LEFT JOIN MASTER_PAJAK_ONLINE_DKI.dbo.TBL_NOPD nop ON REPLACE(nop.nopd,'.','') = kredit.nopd";

    $double = DB::connection("monitoring")
      ->select($query);

    return response()->json(["double_debet" => $double], 200);
  }
}
