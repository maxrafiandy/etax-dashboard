<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;

class UserController extends Controller
{
  use ThrottlesLogins;
  private $menu = array(
    ["type" => "header", "label" => "MAIN"],
    ["type" => "menu", "url" => "/", "icon" => "fa fa-dashboard", "label" => "Dashboard"],
    ["type" => "menu", "url" => "/availability-perangkat", "icon" => "fa fa-map-pin", "label" => "Availability"],

    ["type" => "header", "label" => "REPORT"],
    ["type" => "menu", "url" => "/laporan-autodebet", "icon" => "fa fa-credit-card", "label" => "Lap. Autodebet"],
    ["type" => "menu", "url" => "/laporan-progres-omset", "icon" => "fa fa-line-chart", "label" => "Lap. Progres Omset"],
    ["type" => "menu", "url" => "/laporan-penarikan", "icon" => "fa fa-money", "label" => "Lap. Penarikan"],

    ["type" => "header", "label" => "MAINTENANCE"],
    ["type" => "menu", "url" => "/laporan-offline", "icon" => "fa fa-exclamation-triangle", "label" => "Perangkat Offline"]
  );

  public $maxAttempts = 3;
  public $decayMinutes = 1;

  public function username()
  {
    return 'email';
  }

  public function login(Request $request)
  {
    if ($this->hasTooManyLoginAttempts($request)) {
      $this->fireLockoutEvent($request);
      return response()->json(['error' => 'Too many login attempts.'], 403);
    }

    $email_credentials = $request->only('email', 'password');
    if (Auth::guard("web")->attempt($email_credentials)) {
      $user = Auth::guard("web")->user();
      $auth = $user->createToken($request->email);

      if ($request->remember_me) {
        $auth->token->expires_at = \Carbon\Carbon::now()->addHour(48);
      }

      if ($user->level == "ADMIN") {
        array_push($this->menu, array("type" => "menu", "url" => "/laporan-double-debet", "icon" => "fa fa-money", "label" => "Double Debet"));
        array_push($this->menu, array("type" => "menu", "url" => "/register", "icon" => "fa fa-users", "label" => "Register"));
      }

      return response()->json([
        "token" => $auth->accessToken, "username" => $user->name,
        "level" => $user->level,
        "menu" => $this->menu,
        "wilayah" => $this->getWilayah($user->suban)
      ], 200);
    }

    //keep track of login attempts from the user.
    $this->incrementLoginAttempts($request);

    return response()->json(['error' => 'Credentials do not match. Please try again.'], 401);
  }

  public function logout()
  {
    if (Auth::check()) {
      Auth::user()->AauthAcessToken()->delete();
    }
  }

  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => ['required', 'max:255', 'regex:/[A-Za-z]/'],
      'email' => ['required', 'email'],
      'password' => ['required', 'min:8', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*[!@#$%^&*-+_=()<>?~]).{8,}$/'],
      'c_password' => ['required', 'same:password'],
      'level' => ['required'],
      'suban' => ['required', 'numeric']
    ]);

    if ($validator->fails()) {
      return response()->json(['error' => $validator->errors()], 403);
    }

    $user = Auth::user();

    if ($user->level == "ADMIN") {
      $input = $request->all();
      $input['password'] = bcrypt($input['password']);
      $user = User::create($input);
      $success['token'] =  $user->createToken($user->email)->accessToken;
      $success['name'] =  $user->name;
      return response()->json(['success' => $success], 200);
    }

    return response()->json(['error' => 'Unauthorised'], 401);
  }

  public function updatePassword(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'old_password' => ['required'],
      'password' => ['required', 'min:8', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*[!@#$%^&*-+_=()<>?~]).{8,}$/'],
      'c_password' => ['required', 'same:password'],
    ]);

    if ($validator->fails()) {
      return response()->json(['error' => $validator->errors()], 406);
    }

    $auth = Auth::user();

    if (Auth::guard("web")->attempt([
      "email" => $auth->email,
      "password" => $request->old_password
    ])) {
      $user = User::find($auth->id);
      $user->password = bcrypt($request->password);
      $user->save();
      return response()->json(['user' => $user], 200);
    }

    return response()->json(['error' => 'Unauthorised'], 401);
  }

  public function details()
  {
    $user = Auth::user();
    if ($user->level == "ADMIN") {
      array_push($this->menu, array("type" => "menu", "url" => "/laporan-double-debet", "icon" => "fa fa-money", "label" => "Double Debet"));
      array_push($this->menu, array("type" => "menu", "url" => "/register", "icon" => "fa fa-users", "label" => "Register"));
    }
    $user->menu = $this->menu;
    $user->wilayah = $this->getWilayah($user->suban); 
    return response()->json(['user' => $user], 200);
  }

  private function getWilayah($suban) {
      switch ($suban) {
        case 0:
          return array(
            ["id" => "01", "wilayah" => "JAKARTA PUSAT", "kode" => "P"],
            ["id" => "03", "wilayah" => "JAKARTA SELATAN", "kode" => "S"],
            ["id" => "05", "wilayah" => "JAKARTA TIMUR", "kode" => "T"],
            ["id" => "07", "wilayah" => "JAKARTA BARAT", "kode" => "B"],
            ["id" => "09", "wilayah" => "JAKARTA UTARA", "kode" => "U"]
          );
        case 1:
          return array(
            ["id" => "01", "wilayah" => "JAKARTA PUSAT", "kode" => "P"]
          );
        case 3:
          return array(
            ["id" => "03", "wilayah" => "JAKARTA SELATAN", "kode" => "S"]
          );
        case 5:
          return array(
            ["id" => "05", "wilayah" => "JAKARTA TIMUR", "kode" => "T"]
          );
        case 7:
          return array(
            ["id" => "07", "wilayah" => "JAKARTA BARAT", "kode" => "B"],
          );
        case 9:
          return array(
            ["id" => "09", "wilayah" => "JAKARTA UTARA", "kode" => "U"]
          );
        default: return array([]);
      }
  }
}
