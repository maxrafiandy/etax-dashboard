<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// use App\KoordinatWp;
use App\Nopd;
use App\MirrorProgresOmset;
use DB;

class DashboardController extends Controller
{
  public function sebaranJenisPajak()
  {
    $response = new \stdClass();
    /**********
     * SEBARAN JENIS PAJAK
     * Database source : DWH 
     * MASTER_PAJAK_ONLINE_DKI.dbo.TBL_NOPD, lihat field KODE_KLASIFIKASI_USAHA 
     * (R : Restoran, H : Hiburan, P : Parkir, T : Hotel)
     **********/
    $response->sebaran_jenis_pajak = Nopd::select(
      "KODE_JENIS_USAHA as kode_jenis_usaha",
      DB::raw("COUNT(KODE_JENIS_USAHA) AS jumlah_jenis_usaha")
    )->whereIn("STATUS_LIVE", [4, 5, 6])
      ->groupBy("KODE_JENIS_USAHA")->get();
    // kirim response
    return response()->json($response, 200);
  }

  public function persenOffOn()
  {
    $response = new \stdClass();
    /**********
     * PERSENTASE OFFLINE & ONLINE
     * Joinkan tabel TBL_NOPD yg STATUS_LIVE in (4,5,6) dengan tabel Koordinat berdasarkan NOPD
     * Status 4 : terpasang
     * Status 5 : terkoneksi
     * Status 6 : autodebet
     **********/
    $persen_off_on = Nopd::select(
      "STATUS_LIVE as status_live",
      DB::raw("COUNT(STATUS_LIVE) AS jumlah_status_live"),
      DB::raw("CASE STATUS_LIVE
            WHEN 4 THEN 'TERPASANG'
            WHEN 5 THEN 'TERKONEKSI'
            WHEN 6 THEN 'AUTODEBET'
            END AS keterangan")
    );
    $response->persen_off_on = $persen_off_on->whereIn("STATUS_LIVE", [4, 5, 6])
      ->orderBy("STATUS_LIVE")
      ->groupBy("STATUS_LIVE")->get();
    // kirim response
    return response()->json($response, 200);
  }

  public function offOnPerSudin()
  {
    $response = new \stdClass();
    /**********
     * TABEL OFFLINE ONLINE PER SUDIN
     * Database source : DWH & Koordinat
     * Tabel source : tabel Koordinat join dengan
     * MASTER_PAJAK_ONLINE_DKI.dbo.TBL_NOPD join (by KODE_SUDIN)
     * dengan MASTER_PAJAK_ONLINE_DKI.dbo.SUDIN (TBL_SUDIN_PAJAK)
     **********/
    $offline_online_per_sudin = Nopd::select(
      DB::raw("RIGHT('00'+status_live, 2) AS status_live"),
      "tbl_sudin_pajak.kode_sudin",
      "tbl_sudin_pajak.nama_sudin",
      DB::raw("COUNT(status_live) AS jumlah_status_live")
    )->leftJoin("tbl_sudin_pajak", "tbl_sudin_pajak.kode_sudin", "=", "tbl_nopd.kode_sudin")
      ->groupBy(DB::raw("RIGHT('00'+status_live,2)"), "tbl_sudin_pajak.nama_sudin", "tbl_sudin_pajak.kode_sudin")
      ->orderBy("kode_sudin", "ASC")
      ->get();

    $response->offline_online_per_sudin = $offline_online_per_sudin;
    // kirim response
    return response()->json($response, 200);
  }

  public function offlineOnlineSudin($kode_sudin = 0)
  {
    if ($kode_sudin == 0) {
      $status_sudin = DB::connection("monitoring")->table("koordinat_wp as kwp")
        ->leftJoin("master_pajak_online_dki.dbo.tbl_sudin_pajak as sudin", "sudin.kode_sudin", "kwp.kode_sudin")
        ->leftJoin(DB::raw("(SELECT kode_sudin, count(1) as jumlah_offline from koordinat_wp WHERE status=0 group by kode_sudin) wp2"), "wp2.kode_sudin", "kwp.kode_sudin")
        ->leftJoin(DB::raw("(SELECT kode_sudin, count(1) as jumlah_online from koordinat_wp WHERE status=1 group by kode_sudin) wp3"), "wp3.kode_sudin", "kwp.kode_sudin")
        ->select(
          "kwp.kode_sudin",
          "sudin.nama_sudin",
          DB::raw("COALESCE(jumlah_offline, 0) jumlah_offline"),
          DB::raw("COALESCE(jumlah_online, 0) jumlah_online")
          // DB::raw("(SELECT COUNT(kode_sudin) 
          // FROM koordinat_wp wp2 
          // WHERE wp2.status=0 
          // AND wp2.kode_sudin = kwp.kode_sudin) AS jumlah_offline"),
          // DB::raw("(SELECT COUNT(kode_sudin) FROM koordinat_wp wp3 
          // WHERE wp3.status=1 
          // AND wp3.kode_sudin = kwp.kode_sudin) AS jumlah_online")
        )->groupBy("kwp.kode_sudin", "sudin.nama_sudin", "jumlah_offline", "jumlah_online")
        ->get();
      return response()->json(["status_sudin" => $status_sudin], 200);
    }
  }

  public function getOpSudin($kode_sudin)
  {
    $response = new \stdClass();
    $response->sudin = Nopd::select(
      "tbl_nopd.nopd",
      "tbl_nopd.nama_objek_usaha",
      "tbl_nopd.alamat",
      "kwp.status",
      "kwp.last_status",
      DB::raw("CASE tbl_nopd.STATUS_LIVE
                WHEN 4 THEN 'TERPASANG'
                WHEN 5 THEN 'TERKONEKSI'
                WHEN 6 THEN 'AUTODEBET'
                END AS keterangan")
    )->join(\config("database.monitoring") . ".dbo.koordinat_wp as kwp", "kwp.nopd", "tbl_nopd.nopd")
      ->where("tbl_nopd.kode_sudin", $kode_sudin)
      ->whereIn("tbl_nopd.STATUS_LIVE", [4, 5, 6])
      ->get();
    // kirim response
    return response()->json($response, 200);
  }

  public function getLastTransaction()
  {
    $last_transaction = DB::select("SELECT TOP 5 
      kwp.nopd, kwp.nama_objek_usaha, kwp.alamat, ig.last_data 
      FROM koordinat_wp kwp 
      JOIN etax_grabber.dbo.index_grabber ig on ig.nopd = kwp.nopd
      ORDER BY ig.last_data DESC");
    return response()->json(['last_transaction' => $last_transaction], 200);
  }

  public function getTopOmset()
  {
    $top_omset = DB::select("SELECT TOP 5 
      nopd, nama_objek_pajak, omset3 omset 
      FROM tbl_mirror_progres_omset 
      ORDER BY omset3 DESC");
    return response()->json(['top_omset' => $top_omset], 200);
  }

  public function getTopAutodebet()
  {
    $dbomonitor = config("database.monitoring") . ".dbo";
    $dbompod = config("database.mpod") . ".dbo";
    $top_autodebet = DB::select("SELECT TOP 5 
      nop.nopd, nop.nama_objek_usaha, adb.autodebet_3 autodebet
      FROM $dbomonitor.tbl_mirror_autodebet adb
      JOIN $dbompod.tbl_nopd nop ON REPLACE(nop.nopd,'.','') = adb.nopd
      ORDER BY adb.autodebet_3 DESC");
    return response()->json(['top_autodebet' => $top_autodebet]);
  }

  public function getOmsetSudin()
  {
    $res = MirrorProgresOmset::select("nop.kode_sudin", "sdn.nama_sudin", DB::raw("SUM(trx3) AS total_trx"), DB::raw("SUM(omset3) AS total_omset"))
      ->join(config("database.mpod") . ".dbo.tbl_nopd as nop", "nop.nopd", "tbl_mirror_progres_omset.nopd")
      ->join(config("database.mpod") . ".dbo.tbl_sudin_pajak as sdn", "sdn.kode_sudin", "nop.kode_sudin")
      ->groupBy("nop.kode_sudin", "sdn.nama_sudin")
      ->get();
    return response()->json(["omset" => $res], 200);
  }

  public function getOmsetJenisUsaha($kode_jenis_usaha)
  {
    $res = MirrorProgresOmset::select(DB::raw("SUM(trx3) AS total_trx"), DB::raw("SUM(omset3) AS total_omset"))
      ->join(config("database.mpod") . ".dbo.tbl_nopd as nop", "nop.nopd", "tbl_mirror_progres_omset.nopd")
      ->where("kode_jenis_usaha", $kode_jenis_usaha)
      ->groupBy("kode_jenis_usaha")
      ->first();
    return response()->json([
      "total_trx" => $res->total_trx,
      "total_omset" => $res->total_omset
    ], 200);
  }
}
