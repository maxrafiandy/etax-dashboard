<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactHotel extends Model
{
    protected $connection = "pod";
    protected $table = "tbl_fact_hotel";
}
